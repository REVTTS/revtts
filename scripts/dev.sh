npm install -D eslint@latest prettier@latest
npm install -D @revtts/eslint-plugin-license-notice @revtts/eslint-plugin-no-inner-declarations

prettier . --cache --log-level warn --write
eslint ./**/*.js --cache --fix
