import licenseNoticePlugin from "@revtts/eslint-plugin-license-notice";
import noInnerDeclarations from "@revtts/eslint-plugin-no-inner-declarations";

export default [
  {
    files: ["eslint/**/*.js", "src/**/*.js", "test/**/*.js"],
    languageOptions: {
      ecmaVersion: "latest",
      sourceType: "module",
    },
    plugins: {
      "eslint-plugin-license-notice": licenseNoticePlugin,
      "eslint-plugin-no-inner-declarations": noInnerDeclarations,
    },
    rules: {
      "block-scoped-var": "error",
      camelcase: "error",
      "constructor-super": "error",
      "func-name-matching": ["error", "always"],
      "func-names": ["error", "always"],
      "func-style": ["error", "declaration", { allowArrowFunctions: true }],
      "max-lines": ["error", 200],
      "multiline-comment-style": ["error", "starred-block"],
      "new-cap": "error",
      "no-constant-condition": "error",
      "no-dupe-else-if": "error",
      "no-ex-assign": "error",
      "no-import-assign": "error",
      "no-inner-declarations": ["error", "both"],
      "no-irregular-whitespace": "error",
      "no-global-assign": "error",
      "no-new-symbol": "error",
      "no-self-assign": "error",
      "no-setter-return": "error",
      "no-sparse-arrays": "error",
      "no-shadow-restricted-names": "error",
      "no-this-before-super": "error",
      "no-undef-init": "error",
      "no-unused-private-class-members": "error",
      "no-unused-vars": "error",
      "prefer-exponentiation-operator": "error",
      "use-isnan": "error",
      "valid-typeof": "error",
      yoda: "error",

      "eslint-plugin-license-notice/license-notice": ["error", "Apache-2.0"],
      "eslint-plugin-no-inner-declarations/no-inner-declarations": "error",
    },
  },
];
