/*
 * Copyright 2023 Ronald M Zielaznicki
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export const rule = {
  meta: {
    messages: {
      "nested-function-declaration":
        "function declaration not allowed in nested blocks.",
      "nested-variable-declaration-const":
        "const variable declaration not allowed in nested blocks. They should be declared at the start of a scope and a nested let is a sign to create a new function with a new scope",
      "nested-variable-declaration-let":
        "let variable declaration not allowed in nested blocks. A nested let is a sign to create a new function with a new scope",
      "nested-variable-declaration-var":
        "var variable declaration not allowed in nested blocks. var declarations are hoisted to the top of a scope and nested declarations can create confusing results",
    },
    type: "problem",
  },

  create(context) {
    return {
      FunctionDeclaration(node) {
        if (isInnerDeclaration(node) === false) {
          return;
        }
        context.report({
          messageId: "nested-function-declaration",
          node,
        });
      },
      VariableDeclaration(node) {
        if (isInnerDeclaration(node) === false) {
          return;
        }
        switch (node.kind) {
          case "const":
            context.report({
              messageId: "nested-variable-declaration-const",
              node,
            });
            return;
          case "let":
            context.report({
              messageId: "nested-variable-declaration-let",
              node,
            });
            return;
          case "var":
            context.report({
              messageId: "nested-variable-declaration-var",
              node,
            });
            return;
          default:
            return;
        }
      },
    };
  },
};

import { isInnerDeclaration } from "./lib/is_inner_declaration.js";
