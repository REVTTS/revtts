/*
 * Copyright 2023 Ronald M Zielaznicki
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export const isFunction = (node) => {
  switch (node.type) {
    case FUNCTION_DECLARATION:
      return true;
    case FUNCTION_EXPRESSION:
      return true;
    case ARROW_FUNCTION_EXPRESSION:
      return true;
    default:
      return false;
  }
};

import { ARROW_FUNCTION_EXPRESSION } from "../constants/arrow_function_expression.js";
import { FUNCTION_DECLARATION } from "../constants/function_declaration.js";
import { FUNCTION_EXPRESSION } from "../constants/function_expression.js";
