/*
 * Copyright 2023 Ronald M Zielaznicki
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export const isValidParent = (node) => {
  switch (node.type) {
    case PROGRAM:
      return true;
    case STATIC_BLOCK:
      return true;
    case EXPORT_NAMED_DECLARATION:
      return true;
    case EXPORT_DEFAULT_DECLARATION:
      return true;
    default:
      return false;
  }
};

import { EXPORT_DEFAULT_DECLARATION } from "../constants/export_default_declaration.js";
import { EXPORT_NAMED_DECLARATION } from "../constants/export_named_declaration.js";
import { PROGRAM } from "../constants/program.js";
import { STATIC_BLOCK } from "../constants/static_block.js";
