/*
 * Copyright 2023 Ronald M Zielaznicki
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { RuleTester } from "eslint";
import { rule } from "../src/rule.js";

const ruleTester = new RuleTester({ parserOptions: { ecmaVersion: "latest" } });
ruleTester.run("no-inner-declarations", rule, {
  invalid: [
    {
      code: "function foo() { if(true) { const bar = 'hello'; } };",
      options: [],
      errors: [
        {
          message:
            "const variable declaration not allowed in nested blocks. They should be declared at the start of a scope and a nested let is a sign to create a new function with a new scope",
        },
      ],
    },
    {
      code: "function foo() { if(true) { var bar = 'hello'; } };",
      options: [],
      errors: [
        {
          message:
            "var variable declaration not allowed in nested blocks. var declarations are hoisted to the top of a scope and nested declarations can create confusing results",
        },
      ],
    },
    {
      code: "function foo() { if(true) { let bar = 'hello'; } };",
      options: [],
      errors: [
        {
          message:
            "let variable declaration not allowed in nested blocks. A nested let is a sign to create a new function with a new scope",
        },
      ],
    },
    {
      code: "function foo() { if(true) { function bar(){ console.log('hello'); } } };",
      options: [],
      errors: [
        {
          message: "function declaration not allowed in nested blocks.",
        },
      ],
    },
  ],
  valid: [],
});
