/*
 * Copyright 2023 Hasnae Rehioui (https://github.com/viqueen/eslint-plugin/tree/main/src/license-notice-rule)
 * Copyright 2023 Ronald M Zielaznicki
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export const rule = {
  meta: {
    docs: {
      description: "include license header",
    },
    fixable: "code",
    messages: {
      "found-email-template":
        "copyright [email] template found, replace [email] with valid email or remove it from header",
      "found-name-template":
        "copyright [name] template found, replace [name] with copyright holder's name",
      "found-url-template":
        "copyright [url] template found, replace [url] with valid url or remove it from header",
      "found-year-template":
        "copyright [yyyy] template found, replace [yyyy] with copyright year",
      "invalid-config-license-name-required":
        "invalid eslint configuration, license name is required",
      "malformed-license":
        "licence found, but it did not match the expected license.\n  actual: {{actual}}\n  expected: {{expected}}",
      "missing-header": "missing {{license}} header",
      "missing-license": "header comment missing {{license}} license",
    },
    schema: [
      {
        enum: [...SUPPORTED_LICENSES.keys()],
      },
    ],
    type: "suggestion",
  },
  create(context) {
    return {
      Program: handleProgram(context),
    };
  },
};

function handleProgram(context) {
  return (node) => {
    var [licenseName] = context.options;

    if (licenseName === undefined) {
      context.report({
        messageId: "invalid-config-license-name-required",
        node,
      });
      return;
    }

    // .js, .jsx, .ts, and .tsx file extensions
    var pattern = /^.*\.([jt])sx?$/;
    if (context.filename.match(pattern) === false) {
      return;
    }

    if (isMissingHeader(node)) {
      context.report({
        data: { license: licenseName },
        messageId: "missing-header",
        node,
        fix(fixer) {
          fixer.insertTextBefore(
            node,
            [
              "/*",
              " * Copyright [yyyy] [name] <[email]> ([url])",
              " *",
              SUPPORTED_LICENSES.get(licenseName).split("\n").join("\n * "),
              " */",
            ].join("\n"),
          );
        },
      });
      return;
    }

    if (node.comments[0].value.includes("[yyyy]")) {
      context.report({
        messageId: "found-year-template",
        node,
      });
    }

    if (node.comments[0].value.includes("[name]")) {
      context.report({
        messageId: "found-name-template",
        node,
      });
    }

    if (node.comments[0].value.includes("[email]")) {
      context.report({
        messageId: "found-email-template",
        node,
      });
    }

    if (node.comments[0].value.includes("[url]")) {
      context.report({
        messageId: "found-url-template",
        node,
      });
    }

    const expectedSplitComment =
      SUPPORTED_LICENSES.get(licenseName).split("\n");
    const indexOfLicenseStart = node.comments[0].value.indexOf(
      expectedSplitComment[0],
    );
    if (indexOfLicenseStart === -1) {
      context.report({
        data: { license: licenseName },
        messageId: "missing-license",
        node,
      });
      return;
    }

    const actualSplitComment = node.comments[0].value
      .substring(indexOfLicenseStart)
      .split("\n");
    var actualHasExpected;
    for (let i = 0; i < expectedSplitComment.length; i++) {
      actualHasExpected = actualSplitComment[i].includes(
        expectedSplitComment[i],
      );

      if (actualHasExpected === false) {
        context.report({
          data: {
            actual: actualSplitComment[i],
            expected: expectedSplitComment[i],
          },
          messageId: "malformed-license",
          node,
        });
        return;
      }
    }
  };
}

function isMissingHeader(node) {
  if (node.comments.length === 0) {
    return true;
  }

  if (node.comments[0].type !== "Block") {
    return true;
  }

  if (node.comments[0].range[0] !== 0) {
    return true;
  }

  return false;
}

import { SUPPORTED_LICENSES } from "./supported_licenses.js";
