/*
 * Copyright 2023 Ronald M Zielaznicki
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { RuleTester } from "eslint";
import { rule } from "../src/rule.js";

const ruleTester = new RuleTester({ parserOptions: { ecmaVersion: "latest" } });
ruleTester.run("license-notice", rule, {
  invalid: [
    {
      code: "",
      options: [],
      errors: [
        { message: "invalid eslint configuration, license name is required" },
      ],
    },
    {
      code: "",
      options: ["Apache-2.0"],
      errors: [{ message: "missing Apache-2.0 header" }],
    },
    {
      code: "/* */",
      options: ["Apache-2.0"],
      errors: [{ message: "header comment missing Apache-2.0 license" }],
    },
    {
      code: [
        "/*",
        " * Copyright [yyyy] [name] <[email]> ([url])",
        " *",
        ' * Licensed under the Apache License, Version 2.0 (the "License");',
        " * you may not use this file except in compliance with the License.",
        " * You may obtain a copy of the License at",
        " *",
        " *     http://www.apache.org/licenses/LICENSE-2.0",
        " *",
        " * Unless required by applicable law or agreed to in writing, software",
        ' * distributed under the License is distributed on an "AS IS" BASIS,',
        " * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.",
        " * See the License for the specific language governing permissions and",
        " * limitations under the License.",
        " */",
      ].join("\n"),
      options: ["Apache-2.0"],
      errors: [
        {
          message:
            "copyright [yyyy] template found, replace [yyyy] with copyright year",
        },
        {
          message:
            "copyright [name] template found, replace [name] with copyright holder's name",
        },
        {
          message:
            "copyright [email] template found, replace [email] with valid email or remove it from header",
        },
        {
          message:
            "copyright [url] template found, replace [url] with valid url or remove it from header",
        },
      ],
    },
  ],
  valid: [
    {
      code: [
        "/*",
        ' * Licensed under the Apache License, Version 2.0 (the "License");',
        " * you may not use this file except in compliance with the License.",
        " * You may obtain a copy of the License at",
        " *",
        " *     http://www.apache.org/licenses/LICENSE-2.0",
        " *",
        " * Unless required by applicable law or agreed to in writing, software",
        ' * distributed under the License is distributed on an "AS IS" BASIS,',
        " * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.",
        " * See the License for the specific language governing permissions and",
        " * limitations under the License.",
        " */",
      ].join("\n"),
      options: ["Apache-2.0"],
    },
  ],
});
