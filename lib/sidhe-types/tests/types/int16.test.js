import { Int16 } from "../../src/index.js";
import { testInt } from "./lib/int.js";
import { NumericTestCase } from "./lib/numeric.js";

testInt(
  new NumericTestCase(
    Int16,
    "Int16",
    -32768,
    "-32768",
    -32769,
    "-32769",
    32767,
    "32767",
    32768,
    "32768",
  ),
);
