import { test } from "tap";
import {
  $get,
  $set,
  $test,
  $validate,
  OutOfBoundsTypedError,
} from "../../../src/index.js";
import { catchError } from "./functions/catch_error.js";

export class NumericTestCase {
  class = null;
  name = "";
  lowerBound = 0;
  lowerBoundText = "";
  lowerOutBound = 0;
  lowerOutBoundText = "";
  upperBound = 0;
  upperBoundText = "";
  upperOutBound = 0;
  upperOutBoundText = "";

  constructor(_class, name, lb, lbt, lob, lobt, ub, ubt, uob, uobt) {
    this.class = _class;
    this.name = name;
    this.lowerBound = lb;
    this.lowerBoundText = lbt;
    this.lowerOutBound = lob;
    this.lowerOutBoundText = lobt;
    this.upperBound = ub;
    this.upperBoundText = ubt;
    this.upperOutBound = uob;
    this.upperOutBoundText = uobt;
  }
}

export const testNumericGetter = (testCase) => {
  test(`${testCase.name}.prototype[@@get]`, (t) => {
    const intInstance = new testCase.class(1);

    t.equal(intInstance[$get](), 1, "gets (1)");
    t.equal(typeof intInstance[$get](), "number", 'typeof "number" (1)');

    t.end();
  });
};

export const testNumericSetter = (testCase) => {
  test(`${testCase.name}.prototype[@@set]`, (t) => {
    const intInstance = new testCase.class(1);

    intInstance[$set](2);
    t.equal(intInstance[$get](), 2, "sets to (2)");
    intInstance[$set](3);
    t.equal(intInstance[$get](), 3, "sets to (3)");

    t.end();
  });
};

export const testNumericTester = (testCase) => {
  test(`${testCase.name}[@@test]`, (t) => {
    t.test("pass", (t) => {
      t.equal(testCase.class[$test](0), true, "(0)");
      t.equal(
        testCase.class[$test](testCase.lowerBound),
        true,
        `lower bound > (${testCase.lowerBoundText})`,
      );
      t.equal(
        testCase.class[$test](testCase.upperBound),
        true,
        `upper bound > (${testCase.upperBoundText})`,
      );

      t.end();
    });

    t.test("fail", (t) => {
      t.equal(
        testCase.class[$test](testCase.lowerOutBound),
        false,
        `lower out bound > (${testCase.lowerOutBoundText})`,
      );
      t.equal(
        testCase.class[$test](testCase.upperOutBound),
        false,
        `upper out bound > (${testCase.upperOutBoundText})`,
      );

      t.end();
    });

    t.end();
  });
};

export const testNumericValidator = (testCase) => {
  test(`${testCase.name}[@@validate]`, (t) => {
    t.test("valid", (t) => {
      let error;
      error = catchError(() => testCase.class[$validate](testCase.lowerBound));
      t.equal(error, null, `lower bound > (${testCase.lowerBoundText})`);
      error = catchError(() => testCase.class[$validate](testCase.upperBound));
      t.equal(error, null, `upper bound > (${testCase.upperBoundText})`);

      t.end();
    });

    t.test("throw", (t) => {
      t.test("OutOfBoundsTypedError", (t) => {
        let error;
        error = catchError(() =>
          testCase.class[$validate](testCase.lowerOutBound),
        );
        t.ok(
          error instanceof OutOfBoundsTypedError,
          `(${testCase.lowerOutBoundText})`,
        );
        error = catchError(() =>
          testCase.class[$validate](testCase.upperOutBound),
        );
        t.ok(
          error instanceof OutOfBoundsTypedError,
          `(${testCase.upperOutBound})`,
        );

        t.end();
      });

      t.end();
    });

    t.end();
  });
};
