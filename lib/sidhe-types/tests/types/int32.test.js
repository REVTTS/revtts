import { Int32 } from "../../src/index.js";
import { testInt } from "./lib/int.js";
import { NumericTestCase } from "./lib/numeric.js";

testInt(
  new NumericTestCase(
    Int32,
    "Int32",
    -2147483648,
    "-2147483648",
    -2147483649,
    "-2147483649",
    2147483647,
    "2147483647",
    2147483648,
    "2147483648",
  ),
);
