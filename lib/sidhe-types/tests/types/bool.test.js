import { test } from "tap";
import { catchError } from "./lib/functions/catch_error.js";
import {
  $get,
  $set,
  $test,
  $validate,
  Bool,
  IncorrectTypeTypedError,
} from "../../src/index.js";

test("Bool.prototype[@@get]", (t) => {
  const bool = new Bool(true);

  t.equal(bool[$get](), true, "gets (true)");
  t.equal(typeof bool[$get](), "boolean", 'typeof "boolean"');

  t.end();
});

test("Bool.prototype[@@set]", (t) => {
  const bool = new Bool(true);

  bool[$set](false);
  t.equal(bool[$get](), false, "sets to (false)");
  bool[$set](true);
  t.equal(bool[$get](), true, "set to (true)");

  t.end();
});

test("Bool[@@test]", (t) => {
  t.test("pass", (t) => {
    t.equal(Bool[$test](false), true, "(false)");
    t.equal(Bool[$test](true), true, "(true)");
    t.equal(Bool[$test](Boolean(false)), true, "Boolean(false)");
    t.equal(Bool[$test](Boolean(true)), true, "Boolean(true)");

    t.end();
  });

  t.test("fail", (t) => {
    t.equal(Bool[$test](0), false, "(0)");
    t.equal(Bool[$test](-0), false, "(-0)");
    t.equal(Bool[$test](1), false, "(1)");
    t.equal(Bool[$test](-1), false, "(-1)");
    t.equal(Bool[$test](""), false, "('')");
    t.equal(Bool[$test](""), false, '("")');
    t.equal(Bool[$test](``), false, "(``)");
    t.equal(Bool[$test]("0"), false, '("0")');
    t.equal(Bool[$test]("1"), false, '("1")');
    t.equal(Bool[$test]([]), false, "([])");
    t.equal(Bool[$test]([1]), false, "([1])");
    t.equal(Bool[$test]([[]]), false, "([[]])");
    t.equal(Bool[$test](null), false, "(null)");
    t.equal(Bool[$test](undefined), false, "(undefined)");

    t.end();
  });

  t.end();
});

test("Bool[@@validate]", (t) => {
  t.test("valid", (t) => {
    let error;
    error = catchError(() => Bool[$validate](true));
    t.equal(error, null, "(true)");
    error = catchError(() => Bool[$validate](false));
    t.equal(error, null, "(false)");
    error = catchError(() => Bool[$validate](Boolean(true)));
    t.equal(error, null, "Boolean(true)");
    error = catchError(() => Bool[$validate](Boolean(false)));
    t.equal(error, null, "Boolean(false)");

    t.end();
  });

  t.test("throw", (t) => {
    t.test("IncorrectTypeTypedError", (t) => {
      let error;
      error = catchError(() => Bool[$validate](1));
      t.ok(error instanceof IncorrectTypeTypedError, "(1)");
      error = catchError(() => Bool[$validate](-1));
      t.ok(error instanceof IncorrectTypeTypedError, "(-1)");
      error = catchError(() => Bool[$validate](0));
      t.ok(error instanceof IncorrectTypeTypedError, "(0)");
      error = catchError(() => Bool[$validate](-0));
      t.ok(error instanceof IncorrectTypeTypedError, "(-0)");
      error = catchError(() => Bool[$validate](""));
      t.ok(error instanceof IncorrectTypeTypedError, "('')");
      error = catchError(() => Bool[$validate](""));
      t.ok(error instanceof IncorrectTypeTypedError, '("")');
      error = catchError(() => Bool[$validate](``));
      t.ok(error instanceof IncorrectTypeTypedError, "(``)");
      error = catchError(() => Bool[$validate]("0"));
      t.ok(error instanceof IncorrectTypeTypedError, '("0")');
      error = catchError(() => Bool[$validate]("1"));
      t.ok(error instanceof IncorrectTypeTypedError, '("1")');
      error = catchError(() => Bool[$validate]([]));
      t.ok(error instanceof IncorrectTypeTypedError, "([])");
      error = catchError(() => Bool[$validate]([1]));
      t.ok(error instanceof IncorrectTypeTypedError, "([1])");
      error = catchError(() => Bool[$validate]([[]]));
      t.ok(error instanceof IncorrectTypeTypedError, "([[]])");
      error = catchError(() => Bool[$validate](null));
      t.ok(error instanceof IncorrectTypeTypedError, "(null)");
      error = catchError(() => Bool[$validate](undefined));
      t.ok(error instanceof IncorrectTypeTypedError, "(undefined)");

      t.end();
    });

    t.end();
  });

  t.end();
});
