import { test } from "tap";
import { $get, $set, $test, str } from "../../src/index.js";

test("Str.prototype[@@get]", (t) => {
  const testString = "Hello World";
  const testVal = new (str(testString.length))(testString);

  t.equal(testVal[$get](), "Hello World", 'gets ("Hello World")');
  t.equal(typeof testVal[$get](), "string", 'typeof "string"');
  t.equal(
    testVal[$get]().length,
    testString.length,
    `length is ("Hello World").length`,
  );

  t.end();
});

test("Str.prototype[@@set]", (t) => {
  const testVal = new (str(16))("Hello World");

  testVal[$set]("Foo");
  t.equal(testVal[$get](), "Foo", 'sets to ("Foo")');
  testVal[$set]("Bar");
  t.equal(testVal[$get](), "Bar", 'sets to ("Bar")');

  t.end();
});

test("Str[@@test]", (t) => {
  const Str = str(16);

  t.test("pass", (t) => {
    t.equal(Str[$test](""), true, "('')");
    t.equal(Str[$test](``), true, "(``)");
    t.equal(Str[$test](""), true, '("")');
    t.equal(Str[$test](" "), true, '(" ")');
    t.equal(Str[$test](String("Hello World")), true, '("Hello World")');

    t.end();
  });

  t.test("fail", (t) => {
    t.equal(Str[$test](0), false, "(0)");
    t.equal(Str[$test](null), false, "(null)");
    t.equal(Str[$test](undefined), false, "(undefined)");

    t.end();
  });

  t.end();
});
