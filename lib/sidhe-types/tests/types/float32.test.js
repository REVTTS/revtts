import { Float32 } from "../../src/index.js";
import { testFloat } from "./lib/float.js";
import { NumericTestCase } from "./lib/numeric.js";

testFloat(
  new NumericTestCase(
    Float32,
    "Float32",
    -16777215,
    "-16777215",
    -16777216,
    "-16777216",
    16777215,
    "16777215",
    16777216,
    "16777216",
  ),
);
