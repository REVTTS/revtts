/*
 * Copyright 2023 Ronald M Zielaznicki <ronald.m.zielaznicki@git.revtts.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export class Float64 extends Float {
  static [$byteLength] = Float64Array.BYTES_PER_ELEMENT;
  static [$lowerBound] = Number.MIN_SAFE_INTEGER + 1;
  static [$upperBound] = Number.MAX_SAFE_INTEGER - 1;
  static [$test] = Float[$test].bind(
    undefined,
    this[$lowerBound],
    this[$upperBound],
  );
  static [$validate] = Float[$validate].bind(
    undefined,
    this[$lowerBound],
    this[$upperBound],
    "Float64",
    "double",
  );

  [$byteLength] = Float64[$byteLength];
  [$test] = Float64[$test];
  [$validate] = Float64[$validate];

  constructor(initVal, dataView, offset, owner) {
    super();
    this[$setup](initVal, dataView, offset, owner);
  }

  [$getHook]() {
    return roundToPrecision(this[$dataView].getFloat64(0), 14);
  }

  [$setHook](newVal) {
    this[$dataView].setFloat64(0, newVal);
  }
}

import { roundToPrecision } from "../../lib/types/float/functions/round_to_precision.js";
import { Float } from "../../lib/types/float/float.js";

import { $lowerBound } from "../../lib/types/num/symbols/lower_bound.js";
import { $upperBound } from "../../lib/types/num/symbols/upper_bound.js";

import { $byteLength } from "../../lib/types/typed/symbols/byte_length.js";
import { $dataView } from "../../lib/types/typed/symbols/data_view.js";
import { $getHook } from "../../lib/types/typed/symbols/get_hook.js";
import { $setup } from "../../lib/types/typed/symbols/setup.js";
import { $setHook } from "../../lib/types/typed/symbols/set_hook.js";
import { $test } from "../../lib/types/typed/symbols/test.js";
import { $validate } from "../../lib/types/typed/symbols/validate.js";
