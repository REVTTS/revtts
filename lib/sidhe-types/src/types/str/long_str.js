/*
 * Copyright 2023 Ronald M Zielaznicki <ronald.m.zielaznicki@git.revtts.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const encoder = new TextEncoder();

export const longStr = (numBytes) => {
  return class Str extends _Str {
    static [$byteLength] = numBytes;
    static [$test] = _Str[$test].bind(undefined, numBytes);
    static [$validate] = _Str[$validate].bind(undefined, numBytes);

    [$byteLength] = Str[$byteLength];
    [$test] = Str[$test];
    [$validate] = Str[$validate];

    constructor(initVal, dataView, offset, owner) {
      super();
      this[$setup](initVal, dataView, offset, owner);
    }

    /*
     * as of 2023-11-16
     * larger string (n > ~500) encode faster with TextEncoder
     */
    [$setHook](newVal) {
      encoder.encodeInto(newVal, this[$typedView]);
      super[$resetTypeArray](newVal);
    }
  };
};

import { Str as _Str } from "./classes/str.js";

import { $resetTypeArray } from "./symbol/reset_type_array.js";

import { $byteLength } from "../../lib/types/typed/symbols/byte_length.js";
import { $setup } from "../../lib/types/typed/symbols/setup.js";
import { $setHook } from "../../lib/types/typed/symbols/set_hook.js";
import { $test } from "../../lib/types/typed/symbols/test.js";
import { $typedView } from "../../lib/types/typed/symbols/typed_view.js";
import { $validate } from "../../lib/types/typed/symbols/validate.js";
