/*
 * Copyright 2023 Ronald M Zielaznicki <ronald.m.zielaznicki@git.revtts.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const decoder = new TextDecoder("utf-8", { fatal: true });

export class Str extends TypedPrimitive {
  static [$test](maxLength, val) {
    return (
      (typeof val === "string" || val instanceof String) &&
      val.length <= maxLength
    );
  }

  static [$validate](maxLength, val) {
    if ((typeof val === "string" || val instanceof String) === false) {
      throw new IncorrectTypeTypedError("Str", "string", val);
    }

    if (val.length > maxLength) {
      throw new StrOutOfBoundsTypedError(newVal, newVal.length, maxLength);
    }
  }

  [$getHook]() {
    return decoder.decode(this[$typedView]);
  }

  [$resetTypeArray](newVal) {
    if (newVal.length !== this[$typedView].length) {
      this[$typedView] = new Uint8Array(
        this[$dataView].buffer,
        this[$dataView].byteOffset,
        newVal.length,
      );
    }
  }
}

import { TypedPrimitive } from "../../../lib/types/typed/primitive.js";
import { IncorrectTypeTypedError } from "../../../lib/types/typed/errors/incorrect_type.js";
import { StrOutOfBoundsTypedError } from "../errors/out_of_bounds.js";
import { $resetTypeArray } from "../symbol/reset_type_array.js";

import { $dataView } from "../../../lib/types/typed/symbols/data_view.js";
import { $getHook } from "../../../lib/types/typed/symbols/get_hook.js";
import { $test } from "../../../lib/types/typed/symbols/test.js";
import { $typedView } from "../../../lib/types/typed/symbols/typed_view.js";
import { $validate } from "../../../lib/types/typed/symbols/validate.js";
