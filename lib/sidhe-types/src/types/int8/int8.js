/*
 * Copyright 2023 Ronald M Zielaznicki <ronald.m.zielaznicki@git.revtts.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export class Int8 extends Int {
  static [$byteLength] = Int8Array.BYTES_PER_ELEMENT;
  static [$lowerBound] = -128;
  static [$upperBound] = 127;
  static [$test] = Int[$test].bind(
    undefined,
    this[$lowerBound],
    this[$upperBound],
  );
  static [$validate] = Int[$validate].bind(
    undefined,
    this[$lowerBound],
    this[$upperBound],
    "Int8",
    "integer",
  );

  [$byteLength] = Int8[$byteLength];
  [$test] = Int8[$test];
  [$validate] = Int8[$validate];

  constructor(initVal, dataView, offset, owner) {
    super();
    this[$setup](initVal, dataView, offset, owner);
  }

  [$getHook]() {
    return this[$dataView].getInt8(0);
  }

  [$setHook](newVal) {
    this[$dataView].setInt8(0, newVal);
  }
}

import { Int } from "../../lib/types/int/int.js";

import { $lowerBound } from "../../lib/types/num/symbols/lower_bound.js";
import { $upperBound } from "../../lib/types/num/symbols/upper_bound.js";

import { $byteLength } from "../../lib/types/typed/symbols/byte_length.js";
import { $dataView } from "../../lib/types/typed/symbols/data_view.js";
import { $getHook } from "../../lib/types/typed/symbols/get_hook.js";
import { $setup } from "../../lib/types/typed/symbols/setup.js";
import { $setHook } from "../../lib/types/typed/symbols/set_hook.js";
import { $test } from "../../lib/types/typed/symbols/test.js";
import { $validate } from "../../lib/types/typed/symbols/validate.js";
