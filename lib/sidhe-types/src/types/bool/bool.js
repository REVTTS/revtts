/*
 * Copyright 2023 Ronald M Zielaznicki <ronald.m.zielaznicki@git.revtts.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export class Bool extends TypedPrimitive {
  static [$byteLength] = Uint8Array.BYTES_PER_ELEMENT;

  static [$test](val) {
    return typeof val === "boolean" || val instanceof Boolean;
  }

  static [$validate](val) {
    if (this[$test](val) === false) {
      throw new IncorrectTypeTypedError("Bool", "boolean", val);
    }
  }

  [$byteLength] = Bool[$byteLength];
  [$test] = Bool[$test];
  [$validate] = Bool[$validate];

  constructor(initVal, dataView, offset, owner) {
    super();
    this[$setup](initVal, dataView, offset, owner);
  }

  [$getHook]() {
    return Boolean(this[$typedView][0]);
  }

  [$setHook](newVal) {
    this[$typedView][0] = newVal;
  }
}

import { TypedPrimitive } from "../../lib/types/typed/primitive.js";
import { IncorrectTypeTypedError } from "../../lib/types/typed/errors/incorrect_type.js";

import { $byteLength } from "../../lib/types/typed/symbols/byte_length.js";
import { $getHook } from "../../lib/types/typed/symbols/get_hook.js";
import { $setup } from "../../lib/types/typed/symbols/setup.js";
import { $setHook } from "../../lib/types/typed/symbols/set_hook.js";
import { $test } from "../../lib/types/typed/symbols/test.js";
import { $typedView } from "../../lib/types/typed/symbols/typed_view.js";
import { $validate } from "../../lib/types/typed/symbols/validate.js";
