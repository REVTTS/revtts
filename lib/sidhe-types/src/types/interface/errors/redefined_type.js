/*
 * Copyright 2023 Ronald M Zielaznicki <ronald.m.zielaznicki@git.revtts.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export class RedefinedTypeInterfaceError extends InterfaceError {
  constructor(propName, className, existingTypeName, newTypeName) {
    super(
      `${className} attempted to overwrite property (${propName}) type definition (${existingTypeName}) to incompatible definition ${newTypeName}`,
    );
  }
}

import { InterfaceError } from "./interface_error.js";
