/*
 * Copyright 2023 Ronald M Zielaznicki <ronald.m.zielaznicki@git.revtts.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export function defineProperty(target, propName, descriptor) {
  if (target[$typedProperties].has(propName)) {
    if (descriptor.value?.prototype instanceof TypedPrimitive) {
      return redefineTypedProperty(target, propName, descriptor);
    }
    if (target[$typeIsInit] === false) {
      target[$setup]();
      Object.preventExtensions(target);
    }
    if (descriptor.value !== undefined) {
      target[propName] = descriptor.value;
    }
    return true;
  }

  if (descriptor.value?.prototype instanceof TypedPrimitive) {
    target[$typedProperties].set(propName, descriptor.value);
    return true;
  }

  throw new MissingTypeDefInterfaceError(target.constructor.name, propName);
}

import { redefineTypedProperty } from "./lib/handleTypeRedefinition.js";

import { MissingTypeDefInterfaceError } from "../../../../types/interface/errors/missing_type_def.js";
import { $typedProperties } from "../symbols/typed_properties.js";

import { TypedPrimitive } from "../../typed/primitive.js";
import { $setup } from "../../typed/symbols/setup.js";
import { $typeIsInit } from "../../typed/symbols/type_is_init.js";
