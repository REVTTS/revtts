/*
 * Copyright 2023 Ronald M Zielaznicki <ronald.m.zielaznicki@git.revtts.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export class Typed {
  [$byteLength] = 0;
  [$dataView] = null;
  [$owner] = null;
  [$typedView] = null;
  [$typeIsInit] = false;
  [$test] = null;
  [$validate] = null;

  [$claim](dataView, offset, newOwner) {
    if (this[$owner] !== this) {
      throw new TypedClaimError(
        this.constructor.name,
        this[$owner].constructor.name,
        newOwner.constructor.name,
      );
    }

    this[$dataView] = new DataView(
      dataView.buffer,
      dataView.byteOffset + offset,
      this[$byteLength],
    );
    const newTypedView = new Uint8Array(
      this[$dataView].buffer,
      this[$dataView].byteOffset,
      this[$dataView].byteLength,
    );
    if (this[$typeIsInit] === true) {
      newTypedView.set(this[$typedView]);
    }
    this[$typedView] = newTypedView;
    this[$owner] = newOwner;
  }

  [$getInternal]() {
    throw new UninitializedGetTypedRefError(this.constructor.name);
  }

  [$getHook]() {
    throw new PropertyNotImplementedTypedRefError(
      "_get",
      this.constructor.name,
    );
  }

  [$get]() {
    return this[$getInternal]();
  }

  [$reclaim](dataView, owner) {
    if (this[$owner] !== owner) {
      throw new TypedClaimError(
        this.constructor.name,
        this[$owner].constructor.name,
        owner.constructor.name,
      );
    }

    this[$dataView] = new DataView(
      dataView.buffer,
      this[$dataView].byteOffset,
      this[$dataView].byteLength,
    );
    const newTypedView = new Uint8Array(
      this[$dataView].buffer,
      this[$dataView].byteOffset,
      this[$dataView].byteLength,
    );
    this[$typedView] = newTypedView;
  }

  [$setInternal](newVal) {
    this[$setHook](newVal);
    this[$typeIsInit] = true;
    this[$getInternal] = this[$getHook];
    this[$setInternal] = this[$setHook];
  }

  [$setHook]() {
    throw new PropertyNotImplementedTypedRefError(
      "_set",
      this.constructor.name,
    );
  }

  [$set](newVal) {
    this[$validate](newVal);
    this[$setInternal](newVal);
  }

  [$setup](initVal, dataView, offset, owner) {
    if (dataView === undefined) {
      this[$owner] = this;
      this[$dataView] = new DataView(
        new ArrayBuffer(this[$byteLength]),
        0,
        this[$byteLength],
      );
    } else {
      this[$owner] = owner;
      this[$dataView] = new DataView(
        dataView.buffer,
        dataView.byteOffset + offset,
        this[$byteLength],
      );
    }
    this[$typedView] = new Uint8Array(
      this[$dataView].buffer,
      this[$dataView].byteOffset,
      this[$dataView].byteLength,
    );

    if (initVal === undefined) {
      this[$typeIsInit] = false;
    } else {
      this[$set](initVal);
      this[$typeIsInit] = true;
    }
  }

  [$unclaim](currOwner) {
    if (this[$owner] !== currOwner) {
      throw new TypedUnclaimError(
        this.constructor.name,
        this[$owner].constructor.name,
        currOwner.constructor.name,
      );
    }
    this[$dataView] = new DataView(new ArrayBuffer(this[$byteLength]));
    const newTypedView = new Uint8Array(
      this[$dataView].buffer,
      this[$dataView].byteOffset,
      this[$dataView].byteLength,
    );

    if (this[$typeIsInit] === true) {
      newTypedView[$set](this[$typedView]);
      this[$typedView] = newView;
    }
    this[$typedView] = newTypedView;
    this[$owner] = this;
  }

  toString() {
    return String(this[$get]());
  }
}

import { PropertyNotImplementedTypedRefError } from "./errors/property_not_implemented.js";
import { TypedClaimError } from "./errors/typed_claim_error.js";
import { TypedUnclaimError } from "./errors/typed_unclaim_error.js";
import { UninitializedGetTypedRefError } from "./errors/uninitialized_get.js";

import { $byteLength } from "./symbols/byte_length.js";
import { $claim } from "./symbols/claim.js";
import { $dataView } from "./symbols/data_view.js";
import { $get } from "./symbols/get.js";
import { $getHook } from "./symbols/get_hook.js";
import { $getInternal } from "./symbols/get_internal.js";
import { $owner } from "./symbols/owner.js";
import { $reclaim } from "./symbols/reclaim.js";
import { $set } from "./symbols/set.js";
import { $setHook } from "./symbols/set_hook.js";
import { $setInternal } from "./symbols/set_internal.js";
import { $setup } from "./symbols/setup.js";
import { $test } from "./symbols/test.js";
import { $typedView } from "./symbols/typed_view.js";
import { $typeIsInit } from "./symbols/type_is_init.js";
import { $unclaim } from "./symbols/unclaim.js";
import { $validate } from "./symbols/validate.js";
