# Sidhe Types

It is advised to keep your word when dealing with the sidhe. The consequences laid upon oath breakers is terrible.

## Status

🚧 Under Construction 🚧

Many of the features described in this readme are in active development. This readme is meant to showcase what the library _will_ do, not its current state.

## Why?

Because i'm tired of hearing static.

Typescript, Flow, or even [ECMAScript's type annotations proposal](https://github.com/tc39/proposal-type-annotations). Many flavors, all static.

### The problem(s):

#### 1. Reality Divergence

Existing static languages are mirrors. They are but reflections of the truth. As they are now, type annotations express intent rather than reality. While intent is useful, I think we can do better.

#### 2. Validations lack specificity

You can not create specific narrow types. As an example, try to create an integer type. You can alias number to integer. But, an alias will not error for numbers passed into an integer variable since they are the same. Or, you can create an opaque type and cast declarations into it but that runs us back into point 1.

#### 3. Transpiling

At time of writing, no browser runs a typed just in time language natively. And few server side runtimes exist for typescript.

This would not be a problem if statically typed languages dedicated themselves to backwards compatibility as ECMAScript does. But that is not the case. You can see [Typescript breaking changes](https://github.com/Microsoft/TypeScript/wiki/Breaking-Changes) and [Flow Changelog](https://github.com/facebook/flow/blob/main/Changelog.md) for examples.

## Features

### Stronger runtime typing

Will throw a `TypedError` if a value is not of the expected type. And you'll know exactly where the assignment was attempted, what the value was, and what the expected value should be.

### Serialization

Supports encoding data into [messagepack](https://msgpack.org/). Think of messagepack as JSON's slimmer binary encoding sibling.

### Transferrable

Types are [Transferrable Objects](https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API/Transferable_objects). Moving data to or from a web worker won't require heavy overhead. You can just transfer the ownership or use a `SharedArrayBuffer`.

### Memory Efficiency

Using stronger types allows us to store our data together in `ArrayBuffers`. Using the same buffer to represent the same data in multiple views keeps the memory footprint down. We can also take advantage of the CPU caches since our data is kept together, rather than dynamically allocated to different areas of memory.

## Types

| type        | format     | bytes |              lower bound |             upper bound |
| :---------- | :--------- | ----: | -----------------------: | ----------------------: |
| `Bool`      | primitive  |     1 |              `false`/`0` |              `true`/`1` |
| `Float32`   | primitive  |     4 |            `-16,777,215` |            `16,777,216` |
| `Float64`   | primitive  |     8 | `-9,007,199,254,740,990` | `9,007,199,254,740,991` |
| `Int8`      | primitive  |     1 |                   `-128` |                   `127` |
| `Int16`     | primitive  |     2 |                `-32,768` |                `32,767` |
| `Int32`     | primitive  |     4 |         `-2.147,483,648` |         `2,147,483,647` |
| `Uint8`     | primitive  |     1 |                      `0` |                   `255` |
| `Uint16`    | primitive  |     2 |                      `0` |                `65,535` |
| `Uint32`    | primitive  |     4 |                      `0` |         `4,294,967,295` |
|             |            |       |                          |                         |
| `arr`       | byte array |    \* |                        - |                       - |
| `map`       | byte array |    \* |                        - |                       - |
| `str`       | byte array |    \* |                        - |                       - |
|             |            |       |                          |                         |
| `Dataclass` | object     |  \*\* |                        - |                       - |
| `Interface` | object     |  \*\* |                        - |                       - |
|             |            |       |                          |                         |
| `Callable`  | other      |     - |                        - |                       - |
| `ref`       | other      |     - |                        - |                       - |
| `union`     | other      |     - |                        - |                       - |

\* user defined\
\*\* declaration dependent

## Examples

### Dataclass

#### Dataclass - Example - Basic

```javascript
import { Dataclass, Uint8, str } from "sidhe-types";

class MyDataClass extends Dataclass {
  foo = str(8);
  bar = Uint8;
}

const instance1 = new MyDataclass({ foo: "hello", bar: 1 });
// Object { foo: "hello", bar: 1 }
const instance2 = new MyDataclass("world", 3);
// Object { foo: "world", bar: 3 }
const instance3 = new MyDataclass("world", "5");
// Uncaught TypedError
```

### Callable

#### Callable - Example - Basic

```javascript
import { Callable, Uint8, str } from "sidhe-types";

class MyFn extends Callable {
  foo = str(8);
  bar = Uint8;

  [Callable.$returns] = Str;
}
const combinator = new MyFn((foo, bar) => `${foo} & ${bar}`);

combinator("a", 1);
// "a & 1"
combinator("b", "2");
// Uncaught TypedError
```

#### Callable - Example - As a type

```javascript
import { Callable, Interface, Uint8, str } from 'sidhe-types'

class MyFn extends Callable {
  foo = str(8);
  bar = Uint8;

  [Callable.$returns] = Str;
}

class MyInterface extends Interface {
  myFn: MyFn
}

class MyClass extends MyInterface {
  myFn(a, b) { returns `${a} & ${b}` }
}

const myClassInstance = new MyClass()
myClassInstance.myFn('hi', 3)
// "hi & 3"
```

#### Callable - Example - Inheritance

```javascript
import { Callable, Float32, Uint8, str } from "sidhe-types";

class MyFn extends Callable {
  foo = str(8);
  bar = Float32;

  [Callable.$returns] = Str;
}

class MyExtendedFn extends MyFn {
  bar = Uint8;
}
const combinator = new MyFn((foo, bar) => `${foo} & ${bar}`);
const extendedCombinator = new MyExtendedFn((foo, bar) => `${foo} & ${bar}`);

combinator("a", 1);
// "a & 1"
combinator("a", 1.5);
// "a & 1.5"
extendedCombinator("b", 1);
// "b & 1"
extendedCombinator("b", 1.5);
// Uncaught TypedError
```

### Interface

#### Interface - Example - Basic

```javascript
import { Interface, Uint8, str } from "sidhe-types";

class MyInterface extends Interface {
  foo = str(8);
  bar = Uint8;
}

class MyClass extends MyInterface {
  constructor() {
    // Must call super if constructor is defined
    super();

    this.foo = "hello";
    this.bar = 1;
  }
}

const myInstance = new MyClass();
// Object { foo: "hello", bar: 1 }
```

#### Interface - Example - Type Checking

```javascript
import { Interface, Uint8, str } from "sidhe-types";

class MyInterface extends Interface {
  foo = str(8);
  bar = Uint8;
}

class MyOtherClass extends MyInterface {
  foo = "world";
  bar = "7";
}

const myOtherInstance = new MyOtherClass();
// Uncaught TypedError
```

#### Interface - Example - As a Type

```javascript
import { Interface, Uint8, str } from "sidhe-types";

class PersonInterface extends Interface {
  name = str(8);
}
class AddressInterface extends Interface {
  streetNum = Uint8;
}
class HomeInterface extends Interface {
  owner = PersonInterface;
  address = AddressInterface;
}

class Person extends PersonInterface {
  constructor(name) {
    this.name = name;
  }
}
class Address extends AddressInterface {
  constructor(streetNum) {
    this.streetNum = streetNum;
  }
}
class Condo extends HomeInterface {
  constructor(owner, address) {
    this.owner = owner;
    this.address = address;
  }
}

const owner = new Person("Sue Doe");
const address = new Address(123);
const condo = new Condo(owner, address);
// Object { owner: { name: "Sue Doe" }, address: { streetNum: 123 } }
const newOwner = { name: "Essu" };
const newAddress = new Address(456);
const newCondo = new Condo(newOwner, address);
// Uncaught TypedError
```
