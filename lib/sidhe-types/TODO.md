## TODO

- [ ] Other - factor arraybuffer out from validations
- [ ] Primitive - Float - Investigate improvements to floating point precision and safety
- [ ] Byte Array - arr
- [ ] Byte Array - map
- [ ] Object - Dataclass
- [ ] Other - Callable
- [ ] Other - ref
- [ ] Other - union

## Done

- [x] Primitive - Bool
- [x] Primitive - Int8
- [x] Primitive - Int16
- [x] Primitive - Int32
- [x] Primitive - Uint8
- [x] Primitive - Uint16
- [x] Primitive - Uint32
- [x] Primitive - Float32
- [x] Primitive - Float64
- [x] Byte Array - str
- [x] Object - Interface
