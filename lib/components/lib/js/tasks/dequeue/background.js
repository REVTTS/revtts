/** @type {DequeueTask} */
export const dequeueBackgroundTask = cancelIdleCallback;

/** @typedef {import('./types.d.ts').DequeueTask} DequeueTask */
