/** @type {DequeueTask} */
export const dequeueRenderTask = cancelAnimationFrame;

/** @typedef {import('./types.d.ts').DequeueTask} DequeueTask */
