/** @type {DequeueTask} */
export const dequeueShortTask = clearTimeout;

/** @typedef {import('./types.d.ts').DequeueTask} DequeueTask */
