/**
 * Do not use this function directly.
 * Use one of the other debounce functions instead.
 *
 * @param {EnqueueTask} enqueueTask
 * @returns {DebounceTask}
 */
export const _debounceTask =
  (enqueueTask) =>
  /**
   * @param {Task} task
   * @returns {DebouncedTask}
   */
  (task) => {
    /** @type {number | undefined} */
    let timeoutId;

    /** @type {() => number} */
    const debouncedTask = () => {
      if (timeoutId === undefined) {
        timeoutId = enqueueTask(() => {
          task();
          timeoutId = undefined;
        });
      }

      return timeoutId;
    };

    return debouncedTask;
  };

/** @typedef {import('../enqueue/types.js').EnqueueTask} EnqueueTask */
/** @typedef {import('../types.d.ts').Task} Task */
/** @typedef {import('./types.d.ts').DebounceTask} DebounceTask */
/** @typedef {import('./types.d.ts').DebouncedTask} DebouncedTask */
