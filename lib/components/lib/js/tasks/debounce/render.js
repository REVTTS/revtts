import { _debounceTask } from "./debounce.js";
import { enqueueRenderTask } from "../enqueue/render.js";

/**
 * Use this debounce when we are dealing with animations,
 * dom manipulation, etc. This has to do with anything rendered
 * to the webpage that the user sees.
 *
 * Do not use this debounce to handle any task that lasts more than one
 * render frame, even if it's intended for rendering.
 *
 * Use debounceShortTask instead
 * or break the task up into smaller tasks.
 */
export const debounceRenderTask = _debounceTask(enqueueRenderTask);
