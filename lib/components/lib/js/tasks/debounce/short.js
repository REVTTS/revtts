import { _debounceTask } from "./debounce.js";
import { enqueueShortTask } from "../enqueue/short.js";

/**
 * Use this debounce for short-lived background tasks.
 * Anything that is likely to run in less than one render frame,
 * but also don't mind if it's delayed to another frame.
 */
export const debounceShortTask = _debounceTask(enqueueShortTask);
