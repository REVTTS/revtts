import type { Task } from "../types.js";

export type DebouncedTask = () => number;

export type DebounceTask = (callback: Task) => DebouncedTask;
