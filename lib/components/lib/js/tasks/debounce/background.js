import { _debounceTask } from "./debounce.js";
import { enqueueBackgroundTask } from "../enqueue/background.js";

/**
 * Use this debounce when we don't care when the task runs but don't
 * want the task to run multiple times.
 */
export const debounceBackgroundTask = _debounceTask(enqueueBackgroundTask);
