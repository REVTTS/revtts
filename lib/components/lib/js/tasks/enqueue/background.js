/**
 * Use this queue when we don't care when the task runs. If the task
 * would take more than one render frame to run, make a recursive call
 * to queueBackgroundTask.
 *
 * We have one thread to work with. This thread is our rendering thread,
 * task thread, and everything in between. It is important we do not
 * horde the thread for any task. Background or otherwise. So even
 * if the task is a background task, it must get off the thread as
 * soon as possible and yield back to the main event loop.
 *
 * @type {EnqueueTask}
 */
export const enqueueBackgroundTask = requestIdleCallback;

/** @typedef {import('./types.d.ts').EnqueueTask} EnqueueTask */
