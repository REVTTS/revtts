/**
 * Use this queue when we are dealing with animations,
 * dom manipulation, etc. This has to do with anything rendered
 * to the webpage that the user sees.
 *
 * Do not use this queue to handle any task that lasts more than one
 * render frame, even if it's intended for rendering.
 *
 * Use queueBackgroundTask instead or break the task up into smaller tasks.
 *
 * @type {EnqueueTask}
 */
export const enqueueRenderTask = requestAnimationFrame;

/** @typedef {import('./types.d.ts').EnqueueTask} EnqueueTask */
