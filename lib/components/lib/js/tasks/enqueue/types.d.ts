import type { Task } from "../types.d.ts";

export type EnqueueTask = (task: Task) => number;
