/**
 * Use this queue for short-lived tasks.
 * Anything that is likely to run in less than one rendering frame,
 * but won't cause problems if it's delayed to another frame.
 *
 * @type {EnqueueTask}
 */
export const enqueueShortTask = setTimeout;

/** @typedef {import('./types.d.ts').EnqueueTask} EnqueueTask */
