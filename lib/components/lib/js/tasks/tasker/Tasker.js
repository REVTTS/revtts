/**
 * TLDR; It's a priority queue for callbacks with only
 * one item in it.
 *
 * Implemented initially for atom-flex-rows, a task is a
 * wrapper around a callback. This wrapper allows us to keep
 * a reference to a method: task.run() and change the logic
 * of that method as needed.
 *
 * This is beneficial for components that can have multiple
 * events that require the same values to change before the
 * element is rendered.
 *
 * In atom-flex-rows case, we can get window resize and element
 * events change whether the element will have the .atom-flex-row
 * class or the .atom-flex-column class.
 *
 * Rather than push every single change onto the render queue, we
 * instead debounce the call to task.run and change the callback
 * that would be executed when task.run is pulled from the render
 * queue.
 */
export class Tasker {
  /** @type {number} */
  taskId;

  /** @type {VoidFunction} */
  clear;
  /** @type {VoidFunction} */
  runHook;
  /** @type {(newTask: Task) => void} */
  run;

  constructor() {
    this.taskId = -1;
    let task;

    this.clear = () => {
      this.taskId = -1;
      task = undefined;
    };

    this.runHook = () => {
      if (task !== undefined) {
        task();
        this.taskId = -1;
        task = undefined;
      }
    };

    this.run = (newTask) => {
      task = newTask;
      this.runHook();
    };
  }
}

/** @typedef {import('../types.d.ts').Task} Task */
