import { debounceShortTask } from "../debounce/short.js";
import { dequeueShortTask } from "../dequeue/short.js";
import { Tasker } from "./Tasker.js";

export class ShortTasker extends Tasker {
  constructor() {
    super();
    const debouncedTask = debounceShortTask(super.runHook);

    this.clear = () => {
      if (this.taskId === -1) {
        return;
      }
      dequeueShortTask(this.taskId);
      super.clear();
    };

    this.runHook = () => {
      this.taskId = debouncedTask();
    };
  }
}
