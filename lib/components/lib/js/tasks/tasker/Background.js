import { debounceBackgroundTask } from "../debounce/background.js";
import { dequeueBackgroundTask } from "../dequeue/background.js";
import { Tasker } from "./Tasker.js";

export class BackgroundTasker extends Tasker {
  constructor() {
    super();
    const debouncedTask = debounceBackgroundTask(super.runHook);

    this.clear = () => {
      if (this.taskId === -1) {
        return;
      }
      dequeueBackgroundTask(this.taskId);
      super.clear();
    };

    this.runHook = () => {
      this.taskId = debouncedTask();
    };
  }
}

/** @typedef {import('../debounce/types.d.ts').DebouncedTask} DebouncedTask */
