import { debounceRenderTask } from "../debounce/render.js";
import { dequeueRenderTask } from "../dequeue/render.js";
import { Tasker } from "./Tasker.js";

export class RenderTasker extends Tasker {
  constructor() {
    super();
    const debouncedTask = debounceRenderTask(super.runHook);

    this.clear = () => {
      if (this.taskId === -1) {
        return;
      }
      dequeueRenderTask(this.taskId);
      super.clear();
    };

    this.runHook = () => {
      this.taskId = debouncedTask();
    };
  }
}

/** @typedef {import('../debounce/types.d.ts').DebouncedTask} DebouncedTask */
