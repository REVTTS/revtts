/**
 * @param {Element} element
 * @returns {number}
 */
export const getElementClientWidth = (element) => element.clientWidth;
