import { willFlexRowFitX } from "./willFlexRowFitX.js";

/**
 * @param {HTMLElement} element
 * @returns {boolean}
 */
export const willFlexRowCramX = (element) => !willFlexRowFitX(element);
