import { customEventOptions } from "../../customEventOptions.js";

export class WindowResizeXGrowthCustomEvent extends CustomEvent {
  constructor() {
    super(WINDOW_RESIZE_X_GROWTH_CUSTOM_EVENT_NAME, customEventOptions);
  }
}

export const WINDOW_RESIZE_X_GROWTH_CUSTOM_EVENT_NAME =
  "revtts:window-resize-x-growth";

export const WINDOW_RESIZE_X_GROWTH_CUSTOM_EVENT =
  new WindowResizeXGrowthCustomEvent();
