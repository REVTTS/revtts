import { WindowResizeObserver } from "../window/observer.js";
import { WINDOW_RESIZE_X_GROWTH_CUSTOM_EVENT } from "./customEvent.js";
import { WINDOW_RESIZE_X_GROWTH_HANDLER } from "./handler.js";

export const WINDOW_RESIZE_X_GROWTH_OBSERVER = new WindowResizeObserver(
  WINDOW_RESIZE_X_GROWTH_CUSTOM_EVENT,
  WINDOW_RESIZE_X_GROWTH_HANDLER.handleResize,
);
