export class WindowResizeXGrowthHandler {
  #windowInnerWidth = -1;

  /**
   * @param {WindowResizeObserver} eventTarget
   * @returns {void}
   */
  handleResize(eventTarget) {
    if (window.innerWidth > this.#windowInnerWidth) {
      eventTarget.dispatchPretypedEvent();
    }
    this.#windowInnerWidth = window.innerWidth;
  }
}

export const WINDOW_RESIZE_X_GROWTH_HANDLER = new WindowResizeXGrowthHandler();

/** @typedef {import('../window/observer.js').WindowResizeObserver} WindowResizeObserver*/
