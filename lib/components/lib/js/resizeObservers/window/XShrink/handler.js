export class WindowResizeXShrinkHandler {
  #windowInnerWidth = -1;

  /**
   * @param {WindowResizeObserver} eventTarget
   * @returns {void}
   */
  handleResize(eventTarget) {
    if (window.innerWidth < this.#windowInnerWidth) {
      eventTarget.dispatchPretypedEvent();
    }
    this.#windowInnerWidth = window.innerWidth;
  }
}

export const WINDOW_RESIZE_X_SHRINK_HANDLER = new WindowResizeXShrinkHandler();

/** @typedef {import('../window/observer.js').WindowResizeObserver} WindowResizeObserver*/
