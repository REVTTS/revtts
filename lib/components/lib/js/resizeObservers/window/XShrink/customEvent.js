import { customEventOptions } from "../../customEventOptions.js";

export class WindowResizeXShrinkCustomEvent extends CustomEvent {
  constructor() {
    super(WINDOW_RESIZE_X_SHRINK_CUSTOM_EVENT_NAME, customEventOptions);
  }
}

export const WINDOW_RESIZE_X_SHRINK_CUSTOM_EVENT_NAME =
  "revtts:window-resize-x-shrink";

export const WINDOW_RESIZE_X_SHRINK_CUSTOM_EVENT =
  new WindowResizeXShrinkCustomEvent();
