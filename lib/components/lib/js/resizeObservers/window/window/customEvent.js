import { customEventOptions } from "../../customEventOptions.js";

export class WindowResizeCustomEvent extends CustomEvent {
  constructor() {
    super(WINDOW_RESIZE_CUSTOM_EVENT_NAME, customEventOptions);
  }
}

export const WINDOW_RESIZE_CUSTOM_EVENT_NAME = "revtts:window-resize";

export const WINDOW_RESIZE_CUSTOM_EVENT = new WindowResizeCustomEvent();
