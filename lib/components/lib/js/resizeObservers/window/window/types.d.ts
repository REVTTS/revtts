export type AddPretypedEventListener = (
  cb: EventListenerOrEventListenerObject,
  options?: boolean | AddEventListenerOptions,
) => void;

export type RemovePretypedListener = (
  cb: EventListenerOrEventListenerObject,
  options?: boolean | EventListenerOptions,
) => void;
