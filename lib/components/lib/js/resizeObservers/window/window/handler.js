export class WindowResizeHandler {
  /**
   * @param {WindowResizeObserver} eventTarget
   * @returns {void}
   */
  handleResize(eventTarget) {
    eventTarget.dispatchPretypedEvent();
  }
}

export const WINDOW_RESIZE_HANDLER = new WindowResizeHandler();

/** @typedef {import('./observer.js').WindowResizeObserver} WindowResizeObserver */
