import { WINDOW_RESIZE_HANDLER } from "./handler.js";

/**
 * Don't use this class if you just want to observe when the
 * window resizes. Instead use window.addEventListener directly.
 *
 * Use this class when you want to do something before you dispatch
 * the event.
 *
 * For example:
 *   checking if the window has grown or shrunk in the resize handler
 *   before dispatching the event. So that you can send a specialized event
 *   and prevent listeners from having to perform the check themselves.
 */
export class WindowResizeObserver extends window.EventTarget {
  /** @type {AddPretypedEventListener} */
  addPretypedEventListener;
  /** @type {VoidFunction} */
  dispatchPretypedEvent;
  /** @type {RemovePretypedListener} */
  removePretypedListener;

  /**
   * @param {CustomEvent} event
   * @param {(target: WindowResizeObserver) => void} handleResize
   */
  constructor(event, handleResize) {
    super();

    this.addPretypedEventListener = (cb, options) =>
      this.addEventListener(event.type, cb, options);
    this.dispatchPretypedEvent = () => this.dispatchEvent(event);
    this.removePretypedListener = (cb, options) =>
      this.removeEventListener(event.type, cb, options);

    const thisHandleResize = () => handleResize(this);
    window.addEventListener("resize", thisHandleResize);
  }
}

export const WINDOW_RESIZE_OBSERVER = new WindowResizeObserver(
  // @ts-expect-error - Argument of type 'WindowResizeCustomEvent' is not assignable to parameter of type 'CustomEvent<any>'.
  // typescript, go home, you're drunk. It's a subclass of CustomEvent. It works.
  new WindowResizeCustomEvent(),
  WINDOW_RESIZE_HANDLER.handleResize,
);

/** @typedef {import('./types.d.ts').AddPretypedEventListener} AddPretypedEventListener */
/** @typedef {import('./types.d.ts').RemovePretypedListener} RemovePretypedListener */
