import { customEventOptions } from "../customEventOptions.js";

export class ElementResizeCustomEvent extends CustomEvent {
  constructor() {
    super(ELEMENT_RESIZE_CUSTOM_EVENT_NAME, customEventOptions);
  }
}

export const ELEMENT_RESIZE_CUSTOM_EVENT_NAME = "revtts:element-resize";

export const ELEMENT_RESIZE_CUSTOM_EVENT = new ElementResizeCustomEvent();
