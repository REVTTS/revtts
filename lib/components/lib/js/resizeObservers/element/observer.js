import { ELEMENT_RESIZE_CUSTOM_EVENT } from "./customEvent.js";

/**
 * @param {Array<ResizeObserverEntry>} entries
 * @returns {void}
 */
const resizeObserverCb = (entries) => {
  for (let i = 0; i < entries.length; i++) {
    entries[i].target.dispatchEvent(ELEMENT_RESIZE_CUSTOM_EVENT);
  }
};

export const ELEMENT_RESIZE_OBSERVER = new ResizeObserver(resizeObserverCb);
