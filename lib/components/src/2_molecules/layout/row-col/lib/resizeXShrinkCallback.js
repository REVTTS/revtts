import { willFlexRowFitX } from "../../../../../lib/js/element/willFlexRowFitX.js";
import { LAYOUT_COL_CLASS_NAME } from "../../../../1_atoms/layout/col/className.js";
import { LAYOUT_ROW_CLASS_NAME } from "../../../../1_atoms/layout/row/className.js";
import { resizeXCallback } from "./resizeXCallback.js";

/**
 * @param {AtomRowCol} element
 * @returns {void}
 */
export const resizeXShrinkCallback = (element) =>
  resizeXCallback(
    LAYOUT_ROW_CLASS_NAME,
    LAYOUT_COL_CLASS_NAME,
    willFlexRowFitX,
    element,
  );

/** @typedef {import('../mcl-row-col.js').AtomRowCol} AtomRowCol */
