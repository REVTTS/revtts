import { getElementClientWidth } from "../../../../../lib/js/element/getClientWidth.js";

/**
 * @param {string} classOne
 * @param {string} classTwo
 * @param {(element: HTMLElement) => boolean} fitXFn
 * @param {AtomRowCol} element
 * @returns {void}
 */
export const resizeXCallback = (classOne, classTwo, fitXFn, element) => {
  if (element.classList.contains(classOne)) {
    return;
  }

  if (fitXFn(element)) {
    element.classList.remove(classTwo);
    element.classList.add(classOne);
  }
  element.cachedClientWidth = getElementClientWidth(element);
};

/** @typedef {import('../mcl-row-col.js').AtomRowCol} AtomRowCol */
