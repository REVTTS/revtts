import { willFlexRowCramX } from "../../../../../lib/js/element/willFlexRowCramX.js";
import { LAYOUT_COL_CLASS_NAME } from "../../../../1_atoms/layout/col/className.js";
import { LAYOUT_ROW_CLASS_NAME } from "../../../../1_atoms/layout/row/className.js";
import { resizeXCallback } from "./resizeXCallback.js";

/**
 * @param {AtomRowCol} element
 * @returns {void}
 */
export const resizeXGrowthCallback = (element) =>
  resizeXCallback(
    LAYOUT_COL_CLASS_NAME,
    LAYOUT_ROW_CLASS_NAME,
    willFlexRowCramX,
    element,
  );

/** @typedef {import('../mcl-row-col.js').AtomRowCol} AtomRowCol */
