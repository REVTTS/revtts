import { resizeXGrowthCallback } from "./resizeXGrowthCallback.js";
import { resizeXShrinkCallback } from "./resizeXShrinkCallback.js";

/**
 * @param {AtomRowCol} element
 * @returns {void}
 */
export const elementResizeXCallback = (element) => {
  if (element.cachedClientWidth < element.clientWidth) {
    resizeXGrowthCallback(element);
  } else if (element.cachedClientWidth > element.clientWidth) {
    resizeXShrinkCallback(element);
  }
};

/** @typedef {import('../mcl-row-col.js').AtomRowCol} AtomRowCol */
