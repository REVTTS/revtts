import { ELEMENT_RESIZE_CUSTOM_EVENT_NAME } from "../../../../lib/js/resizeObservers/element/customEvent.js";
import { ELEMENT_RESIZE_OBSERVER } from "../../../../lib/js/resizeObservers/element/observer.js";
import { WINDOW_RESIZE_X_GROWTH_OBSERVER } from "../../../../lib/js/resizeObservers/window/XGrowth/observer.js";
import { WINDOW_RESIZE_X_SHRINK_OBSERVER } from "../../../../lib/js/resizeObservers/window/XShrink/observer.js";
import { RenderTasker } from "../../../../lib/js/tasks/tasker/Render.js";
import { MOLECULE_ROW_COL_CLASS_NAME } from "./className.js";
import { elementResizeXCallback } from "./lib/elementResizeXCallback.js";
import { resizeXGrowthCallback } from "./lib/resizeXGrowthCallback.js";
import { resizeXShrinkCallback } from "./lib/resizeXShrinkCallback.js";

export class AtomRowCol extends HTMLDivElement {
  /** @type {number} */
  cachedClientWidth;
  /** @type {VoidFunction} */
  connectedCallback;
  /** @type {VoidFunction} */
  disconnectedCallback;

  constructor() {
    super();

    this.cachedClientWidth = 0; // set in ./lib/resizeXCallback.ts and checked in ./lib/elementResizeXCallback.ts
    const renderTasker = new RenderTasker();

    const thisElementResizeXCallback = () => elementResizeXCallback(this);
    const windowResizeXGrowthCallback = () => resizeXGrowthCallback(this);
    const windowResizeXShrinkCallback = () => resizeXShrinkCallback(this);

    const elementResizeXListener = () =>
      renderTasker.run(thisElementResizeXCallback);
    const windowResizeXGrowthListener = () =>
      renderTasker.run(windowResizeXGrowthCallback);
    const windowResizeXShrinkListener = () =>
      renderTasker.run(windowResizeXShrinkCallback);

    this.connectedCallback = () => {
      this.cachedClientWidth = this.clientWidth;
      this.addEventListener(
        ELEMENT_RESIZE_CUSTOM_EVENT_NAME,
        elementResizeXListener,
      );
      ELEMENT_RESIZE_OBSERVER.observe(this);
      WINDOW_RESIZE_X_GROWTH_OBSERVER.addPretypedEventListener(
        windowResizeXGrowthListener,
      );
      WINDOW_RESIZE_X_SHRINK_OBSERVER.addPretypedEventListener(
        windowResizeXShrinkListener,
      );
    };

    this.disconnectedCallback = () => {
      ELEMENT_RESIZE_OBSERVER.unobserve(this);
      this.removeEventListener(
        ELEMENT_RESIZE_CUSTOM_EVENT_NAME,
        elementResizeXListener,
      );
      WINDOW_RESIZE_X_GROWTH_OBSERVER.removePretypedListener(
        windowResizeXGrowthListener,
      );
      WINDOW_RESIZE_X_SHRINK_OBSERVER.removePretypedListener(
        windowResizeXShrinkListener,
      );
      this.cachedClientWidth = 0;

      renderTasker.clear();
    };
  }
}

window.customElements.define(MOLECULE_ROW_COL_CLASS_NAME, AtomRowCol, {
  extends: "div",
});
