# Cedalion

The simple bundler for simple web components. No template files. Just HTML, CSS, and Javascript (or Typescript)

## Why?

Because I did not want to use a framework nor javascript to make components.

If you want to bundle a set of web components together you used React, Vue, or some other framework. You'd write the component up in a special file type, then pass that through another program to put it all together. There's a lot of magic behind these tools and they are useful. But, if you want to write HTML you couldn't or there was a "yes but..."

There are native web components too, but the way they are typically used is much like the other frameworks. You use javascript to compose the HTML. So, you still bundle your app with javascript because javascript is needed to build the dom.

I believe this is backwards. Javascript should enhance HTML, not depend on it.
