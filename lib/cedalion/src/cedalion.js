import { loadComponents } from "../lib/1_load/load.js";
import { composeComponents } from "../lib/2_compose/compose.js";
import { bundleComponent } from "../lib/3_bundle/bundle.js";
import { finalizeComponent } from "../lib/4_finalize/finalize.js";
import { saveComponent } from "../lib/5_save/save.js";

/**
 * @param {string} inDir the paths housing all the component files. These should be .html files
 * @param {string} outDir where to put the end results
 *
 * @param {object} dependencies
 * @param {existsSync} dependencies.existsSync
 * @param {mkdir} dependencies.mkdir
 * @param {readdir} dependencies.readdir
 * @param {readFile} dependencies.readFile
 * @param {writeFile} dependencies.writeFile
 */
export const cedalion = async (inDir, outDir, dependencies) => {
  const loadedComponents = await loadComponents(inDir, dependencies);
  const composedComponents = composeComponents(loadedComponents);

  // At this point, each component can be built in isolation.
  // So we run let each of them run independently of one another,
  // rather than forcing them to build as a group.
  await _cedalion(composedComponents, outDir, dependencies);
};

/**
 * @param {Array<ComposedComponent>}
 * @param {string}
 * @returns {Promise<Array<MinifiedComponent>>}
 */
const _cedalion = (composedComponents, outDir, dependencies) => {
  return Promise.all([
    composedComponents.map((component) => {
      return bundleComponent(component, dependencies.readFile)
        .then(finalizeComponent)
        .then((component) => saveComponent(component, outDir, dependencies));
    }),
  ]);
};

/** @typedef {import('../lib/2_minify/lib/ComposedComponent.js').ComposedComponent} ComposedComponent */

/** @typedef {import('node:fs').existsSync} existsSync */
/** @typedef {import('node:fs').mkdir} mkdir */
/** @typedef {import('node:fs').readdir} readdir */
/** @typedef {import('node:fs').readFile} readFile */
/** @typedef {import('node:fs').writeFile} writeFile */
