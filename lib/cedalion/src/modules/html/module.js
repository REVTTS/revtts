import { CedalionModule } from "../../../lib/models/Module.js";
import { htmlModuleOnLoad } from "./onLoad.js";

export const htmlModule = new CedalionModule({
  htmlModuleOnLoad,
  onCompose: (webComponent) => webComponent,
  onBundle: (webComponent) => webComponent,
  onFinalize: (webComponent) => webComponent,
  onSave: (webComponent) => webComponent,
});
