import { CedalionModulePromiseFn } from "../../../lib/typing/Module.js";

/** @param {import('node:fs').readFile} readFile */
export const htmlModuleOnLoad = (readFile) =>
  CedalionModulePromiseFn((webComponent) => {
    // Read file from WebComponent.path

    // Handle meta name="component" content="<component-name>"
    // - Extract content of meta tag
    // - Populate WebComponent.name with content
    // - Remove meta tag

    // Handle link rel="import" with data-bundle-external attribute
    // - Extract href path from link
    // - Resolve path to absoluate path
    // - Load file into new WebComponent
    // - Push file to WebComponent.dependencies

    // Handle link rel="import" with data-bundle-inline attribute
    // Handle link rel="import" with data-bundle-internal attribute

    return webComponent;
  });
