import { test } from "tap";
import { WebComponent } from "../../../lib/typing/WebComponent/WebComponent.js";
import { htmlModuleOnLoad } from "./onLoad.js";

const readFileTests = (t) => {
  const webComponent = new WebComponent({
    path: "/my/test/path.html",
  });

  const readFile = (path, options, cb) => {};
};

test("modules.html.onLoad", readFileTests);
