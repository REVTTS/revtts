#!/usr/bin/env node

import { existsSync, mkdir, readdir, readFile, writeFile } from "node:fs";
import { resolve } from "node:path";

import yargs from "yargs";
import { hideBin } from "yargs/helpers";

import { cedalion } from "../src/cedalion.js";

// TODO: Handle bundle-for-render link rel="icon"
//       Need to get the file during the build step and add the link back in.
const args = yargs(hideBin(process.argv))
  .scriptName("cedalion")
  .command("$0 [indir] [outdir]", "", (yargs) => {
    yargs.positional("indir", {
      default: "./src",
    });
    yargs.positional("outdir", {
      default: "./dist",
    });
  })
  .help().argv;

const run = async () => {
  try {
    const indir = resolve(args.indir);
    const outdir = resolve(args.outdir);
    await cedalion(indir, outdir, {
      existsSync,
      mkdir,
      readdir,
      readFile,
      writeFile,
    });
  } catch (e) {
    console.error(e);
  }
};

run();
