import { test } from "tap";

import { generateCss } from "../../../../../lib/ast/css/generate.js";
import { parseCss } from "../../../../../lib/ast/css/parse.js";
import { minifyCssIdenticalDeclarations } from "../../../../../lib/4_finalize/lib/css/identicalDeclarations.js";

const minifyCssIdenticalDeclarationsTest = (t) => {
  let input = parseCss(".foo{color:white}.bar{color:white}");
  let expected = ".bar,.foo{color:white}";
  let actual = generateCss(minifyCssIdenticalDeclarations(input));
  t.equal(actual, expected);

  input = parseCss(".foo{color:white}.bar{color:white}.foo,.bar{color:white}");
  expected = ".bar,.foo{color:white}";
  actual = generateCss(minifyCssIdenticalDeclarations(input));
  t.equal(actual, expected);

  input = parseCss(
    ".foo{background-color:black;color:white}.bar{background-color:black;color:white}",
  );
  expected = ".bar,.foo{background-color:black;color:white}";
  actual = generateCss(minifyCssIdenticalDeclarations(input));
  t.equal(actual, expected);

  t.end();
};

test(
  "minify/lib/css/identicalDeclarations",
  minifyCssIdenticalDeclarationsTest,
);
