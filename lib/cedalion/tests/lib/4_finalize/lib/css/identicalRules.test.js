import { test } from "tap";

import { parseCss } from "../../../../../lib/ast/css/parse.js";
import { generateCss } from "../../../../../lib/ast/css/generate.js";
import { minifyCssIdenticalRules } from "../../../../../lib/4_finalize/lib/css/identicalRules.js";

const identicalRulesTest = (t) => {
  const input = parseCss(
    ".foo-bar{color:white}.foo-bar{background-color:white}",
  );
  const expected = ".foo-bar{color:white;background-color:white}";

  const actual = generateCss(minifyCssIdenticalRules(input));

  t.equal(actual, expected);

  t.end();
};

const rootRulesTest = (t) => {
  const input = parseCss(
    ":root{--gray-30:#ECF1F9}:root{--gray-80:#464360}:root{--white:#fff}",
  );
  const expected = ":root{--gray-30:#ECF1F9;--gray-80:#464360;--white:#fff}";

  const actual = generateCss(minifyCssIdenticalRules(input));

  t.equal(actual, expected);

  t.end();
};

const atRulesTest = (t) => {
  const input = parseCss(
    '@font-face{font-family:"foo"}@font-face{font-family:"bar"}',
  );
  const expected = '@font-face{font-family:"foo"}@font-face{font-family:"bar"}';

  const actual = generateCss(minifyCssIdenticalRules(input));

  t.equal(actual, expected);

  t.end();
};

test("minify/lib/css/identicalCssRules - .class", identicalRulesTest);
test("minify/lib/css/identicalCssRules - :root", rootRulesTest);
test("minify/lib/css/identicalCssRules - @at-rule", atRulesTest);
