import { test } from "tap";

import { parseCss } from "../../../../lib/ast/css/parse.js";
import { getCssClassSelectorNameSet } from "../../../../lib/ast/css/getClassSelectorNameSet.js";
import { areSetsEqual } from "../../../../lib/areSetsEqual.js";

const getCssClassSelectorNameSetTest = (t) => {
  const input = parseCss(".foo{color:white}.bar{color:white}");
  const expected = new Set(["foo", "bar"]);
  const actual = getCssClassSelectorNameSet(input);
  t.ok(areSetsEqual(actual, expected));

  t.end();
};

test("lib/css/getClassSelectorNameSet", getCssClassSelectorNameSetTest);
