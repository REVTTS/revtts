import fs from "node:fs";
import { test } from "tap";
import tmp from "tmp";

import { ComposedComponent } from "../../../../lib/2_compose/lib/ComposedComponent.js";
import { bundleComponentScript } from "../../../../lib/3_bundle/lib/script.js";

const baseTests = (t) => {
  const jsDependencyContent = 'console.log("bar");';
  const tmpFileObject = tmp.fileSync({ postfix: ".ts" });
  fs.writeFileSync(tmpFileObject.name, jsDependencyContent, {
    encoding: "utf-8",
  });
  const composedComponent = new ComposedComponent(
    "<div></div>",
    "foo",
    "./foo.html",
    [],
    [],
    [tmpFileObject.name],
  );

  bundleComponentScript(composedComponent)
    .then((jsFiles) => {
      const builtJs = jsFiles[0].text;
      const jsLines = builtJs.split("\n");
      t.equal(jsLines[1], 'console.log("bar");');
      t.end();
    })
    .catch((e) => {
      console.error(e);
      t.fail();
    });
};

const noDependenciesTests = (t) => {
  const composedComponent = new ComposedComponent(
    "<div></div>",
    "foo",
    "./foo.html",
    [],
    [],
    [],
  );

  bundleComponentScript(composedComponent)
    .then((jsFiles) => {
      t.equal(jsFiles.length, 0);
      t.end();
    })
    .catch((e) => {
      console.error(e);
      t.fail();
    });
};

tmp.setGracefulCleanup();
test("bundle - lib - script", baseTests);
test("bundle - lib - script: no dependencies", noDependenciesTests);
