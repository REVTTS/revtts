import fs from "node:fs";
import { test } from "tap";
import tmp from "tmp";

import { ComposedComponent } from "../../../../lib/2_compose/lib/ComposedComponent.js";
import { bundleComponentCss } from "../../../../lib/3_bundle/lib/css.js";

const baseTests = async (t) => {
  const cssDependencyContent = ".foo-bar { color: white; }";
  const tmpFileObject = tmp.fileSync({ postfix: ".css" });
  fs.writeFileSync(tmpFileObject.name, cssDependencyContent, {
    encoding: "utf-8",
  });
  const composedComponent = new ComposedComponent(
    "<div></div>",
    "foo",
    "./foo.html",
    [tmpFileObject.name],
    [],
    [],
  );

  await bundleComponentCss(composedComponent)
    .then(({ cssFiles }) => {
      const cssFile = cssFiles[0];
      const cssLines = cssFile.text.split("\n");
      t.equal(cssLines[1], ".foo-bar {");
      t.equal(cssLines[2], "  color: white;");
      t.equal(cssLines[3], "}");
    })
    .catch((e) => {
      console.error(e);
      t.fail();
    });
};

const noDependenciesTests = async (t) => {
  const composedComponent = new ComposedComponent(
    "<div></div>",
    "foo",
    "./foo.html",
    [],
    [],
    [],
  );

  await bundleComponentCss(composedComponent)
    .then(({ cssFiles }) => {
      t.equal(cssFiles.length, 0);
    })
    .catch((e) => {
      console.error(e);
      t.fail();
    });
};

tmp.setGracefulCleanup();
test("build - lib - css", baseTests);
test("build - lib - css: no dependencies", noDependenciesTests);
