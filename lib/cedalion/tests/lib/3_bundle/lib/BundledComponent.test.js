import { test } from "tap";

import { BundledComponent } from "./BuiltComponent.js";

test("bundle - lib- BundledComponent", (t) => {
  const bundledComponent = new BundledComponent(
    "foo-name",
    "foo-path",
    "foo-css",
    "foo-html",
    "foo-js",
    [],
  );

  t.equal(bundledComponent.name, "foo-name");
  t.equal(bundledComponent.path, "foo-path");
  t.equal(bundledComponent.css, "foo-css");
  t.equal(bundledComponent.html, "foo-html");
  t.equal(bundledComponent.js, "foo-js");

  t.end();
});
