import { test } from "tap";

import { areSetsEqual } from "../../lib/areSetsEqual.js";
import { parseHtml } from "../../lib/ast/html/parse.js";
import { getHtmlClassSelectorNameSet } from "../../lib/ast/html/getClassSelectorNameSet.js";

const getHtmlClassSelectorNameSetTest = (t) => {
  const input = parseHtml('<input class="foo bar">');
  const expected = new Set(["foo", "bar"]);
  const actual = getHtmlClassSelectorNameSet(input);
  t.ok(areSetsEqual(actual, expected));

  t.end();
};

test("lib/html/getClassSelectorNameSet", getHtmlClassSelectorNameSetTest);
