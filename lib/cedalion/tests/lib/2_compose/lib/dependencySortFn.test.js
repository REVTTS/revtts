import { test } from "tap";

import { LoadedComponent } from "../../../../lib/1_load/lib/LoadedComponent/LoadedComponent.js";
import { dependencySortFn } from "../../../../lib/2_compose/lib/dependencySortFn.js";

const dependencySortOutOfOrderFnTest = (t) => {
  const input = [
    new LoadedComponent("<atom-foo></atom-foo>", "./organism-bar.html"),
    new LoadedComponent("<input></input>", "./atom-foo.html"),
  ];
  const output = input.sort(dependencySortFn);

  t.equal(output[0].name, "atom-foo");
  t.equal(output[1].name, "organism-bar");

  t.end();
};

const dependencySortInOrderFnTest = (t) => {
  const input = [
    new LoadedComponent("<input></input>", "./atom-foo.html"),
    new LoadedComponent("<atom-foo></atom-foo>", "./organism-bar.html"),
  ];

  const output = input.sort(dependencySortFn);

  t.equal(output[0].name, "atom-foo");
  t.equal(output[1].name, "organism-bar");

  t.end();
};

const dependencySortEqualTest = (t) => {
  const input = [
    new LoadedComponent("<input></input>", "./atom-foo.html"),
    new LoadedComponent("<input></input>", "./atom-bar.html"),
  ];

  const output = input.sort(dependencySortFn);

  t.equal(output[0].name, "atom-foo");
  t.equal(output[1].name, "atom-bar");

  t.end();
};

const dependencySortRecursiveDependencyTest = (t) => {
  const input = [
    new LoadedComponent("<atom-bar></atom-bar>", "./atom-foo.html"),
    new LoadedComponent("<atom-foo></atom-foo>", "./atom-bar.html"),
  ];

  t.throws(() => input.sort(dependencySortFn));

  t.end();
};

test(
  "compile - dependencySortFn - out of order",
  dependencySortOutOfOrderFnTest,
);
test("compile - dependencySortFn - in order", dependencySortInOrderFnTest);
test("compile - dependencySortFn - equal", dependencySortEqualTest);
test(
  "compile - dependencySortFn - recursive dependency",
  dependencySortRecursiveDependencyTest,
);
