import { LoadedComponentTestCase } from "./LoadedComponentTestCase.js";

export const testCaseBasicCssDependencies = new LoadedComponentTestCase({
  description: "LoadedComponent - Basic cssDependencies",
  inputHtml:
    '<link bundle-for-render href="./atom-flex-row-column.css" rel="stylesheet" /></script><atom-input></atom-input>',
  inputPath: "./organism-test.html",
  expectedHtml: "<atom-input></atom-input>",
  expectedName: "organism-test",
  expectedComponentDependencies: ["atom-input"],
  expectedCssDependencies: ["./atom-flex-row-column.css"],
  expectedJsDependencies: [],
});
