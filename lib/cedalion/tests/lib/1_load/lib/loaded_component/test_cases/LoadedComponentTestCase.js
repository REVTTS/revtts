import { dirname, resolve } from "node:path";

import { LoadedComponent } from "../LoadedComponent.js";

export class LoadedComponentTestCase {
  description;

  inputHtml;
  inputPath;

  expectedHtml;
  expectedName;

  expectedComponentDependencies;
  expectedCssDependencies;
  expectedJsDependencies;

  /**
   * @param {object} options
   * @param {string} options.description
   * @param {string} options.inputHtml
   * @param {string} options.inputPath
   * @param {string} options.expectedHtml
   * @param {string} options.expectedName
   * @param {Array<string>} options.expectedCssDependencies
   * @param {Array<string>} options.expectedJsDependencies
   */
  constructor({
    description,
    inputHtml,
    inputPath,
    expectedHtml,
    expectedName,
    expectedComponentDependencies,
    expectedCssDependencies,
    expectedJsDependencies,
  }) {
    this.description = description;
    this.inputHtml = inputHtml;
    this.inputPath = resolve(inputPath);

    this.expectedHtml = expectedHtml;
    this.expectedName = expectedName;

    this.expectedComponentDependencies = expectedComponentDependencies;
    const pathDir = dirname(inputPath);
    this.expectedCssDependencies = expectedCssDependencies.map(
      (cssDependencyPath) => resolve(pathDir, cssDependencyPath),
    );
    this.expectedJsDependencies = expectedJsDependencies.map(
      (jsDependencyPath) => resolve(pathDir, jsDependencyPath),
    );
  }

  test(t) {
    t.test(this.description, (t) => {
      const loadedComponent = new LoadedComponent(
        this.inputHtml,
        this.inputPath,
      );

      t.equal(
        loadedComponent.html,
        this.expectedHtml,
        "Populates html from input",
      );
      t.equal(
        loadedComponent.name,
        this.expectedName,
        "Populates name from path",
      );
      t.match(
        loadedComponent.cssBundleForRenderDependencies,
        this.expectedCssDependencies,
        "Populates cssDependencies",
      );
      t.match(
        loadedComponent.jsBundleForRenderDependencies,
        this.expectedJsDependencies,
        "Populates jsDependencies",
      );

      t.end();
    });
  }
}
