import { LoadedComponentTestCase } from "./LoadedComponentTestCase.js";

export const testCaseBasicAtom = new LoadedComponentTestCase({
  description: "LoadedComponent - Basic Atom",
  inputHtml: "<input>",
  inputPath: "./atom-test.html",
  expectedHtml: "<input>",
  expectedName: "atom-test",
  expectedComponentDependencies: [],
  expectedCssDependencies: [],
  expectedJsDependencies: [],
});
