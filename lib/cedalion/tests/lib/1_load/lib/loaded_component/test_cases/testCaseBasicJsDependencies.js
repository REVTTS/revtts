import { LoadedComponentTestCase } from "./LoadedComponentTestCase.js";

export const testCaseBasicJsDependencies = new LoadedComponentTestCase({
  description: "LoadedComponent - Basic jsDependencies",
  inputHtml:
    '<script bundle-for-render src="./atom-flex-row-column.ts" type="module" ></script><atom-input></atom-input>',
  inputPath: "./organism-test.html",
  expectedHtml: "<atom-input></atom-input>",
  expectedName: "organism-test",
  expectedComponentDependencies: ["atom-input"],
  expectedCssDependencies: [],
  expectedJsDependencies: ["./atom-flex-row-column.ts"],
});
