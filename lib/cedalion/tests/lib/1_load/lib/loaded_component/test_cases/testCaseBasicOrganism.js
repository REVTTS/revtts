import { LoadedComponentTestCase } from "./LoadedComponentTestCase.js";

export const testCaseBasicOrganism = new LoadedComponentTestCase({
  description: "LoadedComponent - Basic Organism",
  inputHtml: "<atom-input></atom-input>",
  inputPath: "./organism-test.html",
  expectedHtml: "<atom-input></atom-input>",
  expectedName: "organism-test",
  expectedComponentDependencies: ["atom-input"],
  expectedCssDependencies: [],
  expectedJsDependencies: [],
});
