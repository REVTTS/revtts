import { test } from "tap";

import { testCaseBasicAtom } from "./test_cases/testCaseBasicAtom.js";
import { testCaseBasicOrganism } from "./test_cases/testCaseBasicOrganism.js";
import { testCaseBasicCssDependencies } from "./test_cases/testCaseBasicCssDependencies.js";
import { testCaseBasicJsDependencies } from "./test_cases/testCaseBasicJsDependencies.js";

const loadedComponentTests = (t) => {
  testCases.forEach((testCase) => {
    testCase.test(t);
  });

  t.end();
};

const testCases = [
  testCaseBasicAtom,
  testCaseBasicOrganism,
  testCaseBasicCssDependencies,
  testCaseBasicJsDependencies,
];

test("load - LoadedComponent", loadedComponentTests);
