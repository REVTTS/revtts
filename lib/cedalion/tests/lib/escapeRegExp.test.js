import { test } from "tap";
import { escapeRegExp } from "../../lib/escapeRegExp.js";

const escapeRegExpTest = (t) => {
  const input = "()";
  // This is meant to be regex. So the escape is used.
  // prettier-ignore
  const expected = "\\(\\)";
  const actual = escapeRegExp(input);

  t.equal(actual, expected);

  t.end();
};

test("escapeRegExp", escapeRegExpTest);
