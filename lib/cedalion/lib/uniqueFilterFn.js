/**
 * @param {string} value
 * @param {number} index
 * @param {Array<string>} array
 *
 * @returns {boolean}
 */
export const uniqueFilterFn = (value, index, array) =>
  array.indexOf(value) === index;
