import { BasicModel } from "objectmodel";

export const WebComponentName = BasicModel(String)
  .as("WebComponent.name")
  .assert((name) => VALID_NAME_REGEX.test(name), "is valid custom element name")
  .assert(
    (name) => ILLEGAL_NAMES.includes(name) === false,
    "is not member of list of illegal names",
  );

/** @see {@link https://html.spec.whatwg.org/multipage/custom-elements.html#valid-custom-element-name} */
const PCENChar = String.raw`(?:-|\.|[0-9]|_|a-z|\xB7|[\u00C0-\u00D6]|[\u00D8-\u00F6]|[\u00F8-\u037D]|[\u037F-\u1FFF]|[\u200C-\u200D]|[\u203F-\u2040]|[\u2070-\u218F]|[\u2C00-\u2FEF]|[\u3001-\uD7FF]|[\uF900-\uFDCF]|[\uFDF0-\uFFFD]|[\u{10000}-\u{EFFFF}])`;
const PotentialCustomElementName = String.raw`[a-z]${PCENChar}*-${PCENChar}*`;
const VALID_NAME_REGEX = new RegExp(PotentialCustomElementName, "u");

const ILLEGAL_NAMES = [
  "annotation-xml",
  "color-profile",
  "font-face",
  "font-face-src",
  "font-face-uri",
  "font-face-format",
  "font-face-name",
  "missing-glyph",
];
