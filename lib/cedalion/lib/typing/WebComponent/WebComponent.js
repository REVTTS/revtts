import { ArrayModel, ObjectModel } from "objectmodel";
import { FileObject } from "../FileObject/FileObject.js";
import { DocumentFragment } from "../html/DocumentFragment.js";
import { WebComponentName } from "./name.js";

const WebComponent = ObjectModel({
  dependencies: ArrayModel(undefined),
  documentFragment: [DocumentFragment],
  files: ArrayModel(FileObject),
  name: [WebComponentName],
  path: String,
})
  .as("WebComponent")
  .defaultTo({
    dependencies: [],
    files: [],
  });
WebComponent.definition.dependencies.definition = ArrayModel(WebComponent);

export { WebComponent };
