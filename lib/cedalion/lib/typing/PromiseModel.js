import { BasicModel, Model } from "objectmodel";

export const PromiseModel = (definition) => {
  const PromiseModel = BasicModel(Promise);
  const ResolvedValueModel = Model(definition);
  return (p) => PromiseModel(p).then((x) => ResolvedValueModel(x));
};
