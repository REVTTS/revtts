import { DocumentFragment } from "./DocumentFragment.js";
import { Element } from "./Element.js";

export const Template = Element.extend({
  content: DocumentFragment,
  nodeName: "template",
  tagName: "template",
}).defaultTo({
  nodeName: "template",
  tagName: "template",
});
