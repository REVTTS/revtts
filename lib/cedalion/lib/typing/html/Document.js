import { ObjectModel } from "objectmodel";
import { ChildNodes } from "./ChildNodes.js";
import { Location } from "./Location.js";

export const Document = ObjectModel({
  childNodes: ChildNodes,
  mode: ["no-quirks", "quirks", "limited-quirks"],
  nodeName: "#document",
  sourceCodeLocation: [Location, null, undefined],
});
