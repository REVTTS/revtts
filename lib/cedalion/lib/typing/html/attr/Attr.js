import { ObjectModel } from "objectmodel";
import { HtmlAttrName } from "./Name.js";
import { HtmlAttrValue } from "./Value.js";

export const HtmlAttr = ObjectModel({
  name: HtmlAttrName,
  value: HtmlAttrValue,
}).as("Attr");
