import { BasicModel } from "objectmodel";

export const HtmlAttrValue = BasicModel([String, undefined]).as(
  "HtmlAttrValue",
);
