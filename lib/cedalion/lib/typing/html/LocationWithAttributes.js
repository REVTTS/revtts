import { MapModel } from "objectmodel";
import { Location } from "./Location.js";

export const LocationWithAttributes = Location.extend({
  attrs: [MapModel(String, Location)],
});
