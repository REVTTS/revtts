import { ArrayModel, FunctionModel } from "objectmodel";
import { HtmlAttr, HtmlAttrName, HtmlAttrValue } from "./attr/index.js";

const GetAttrFn = FunctionModel(HtmlAttrName)
  .return([HtmlAttr, undefined])
  .as("getAttr");
const HasAttrFn = FunctionModel(HtmlAttrName).return(Boolean).as("hasAttr");
const RemoveAttrFn = FunctionModel(HtmlAttrName)
  .return(undefined)
  .as("removeAttr");
const SetAttrFn = FunctionModel(HtmlAttrName, HtmlAttrValue)
  .return(undefined)
  .as("setAttr");

class HtmlAttrs extends ArrayModel(HtmlAttr).as("HtmlAttrs") {
  getAttr = GetAttrFn(function getAttr(name) {
    for (let i = 0; i < this.length; i++)
      if (this[i].name === name) return this[i];

    return undefined;
  });

  hasAttr = HasAttrFn(function hasAttr(name) {
    for (let i = 0; i < this.length; i++)
      if (this[i].name === name) return true;

    return false;
  });

  removeAttr = RemoveAttrFn(function removeAttr(name) {
    for (let i = this.length - 1; i > -1; i--)
      if (this[i].name === name) this.splice(i, 1);
  });

  setAttr = SetAttrFn(function setAttr(name, value) {
    this.removeAttr(name);
    this.push({ name, value });
  });
}

export { HtmlAttrs };
