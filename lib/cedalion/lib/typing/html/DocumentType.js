import { ObjectModel } from "objectmodel";
import { Location } from "./Location.js";
import { ParentNode, initParentNode } from "./ParentNode.js";

initParentNode();

export const DocumentType = new ObjectModel({
  name: String,
  nodeName: "#documentType",
  parentNode: ParentNode,
  publicId: String,
  sourceCodeLocation: [Location, null, undefined],
  systemId: String,
}).defaultTo({
  nodeName: "#documentType",
});
