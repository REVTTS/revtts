import { Location } from "./Location.js";
import { LocationWithAttributes } from "./LocationWithAttributes.js";

export const ElementLocation = LocationWithAttributes.extend({
  startTag: [Location],
  endTag: [Location],
});
