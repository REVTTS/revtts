import { test } from "tap";
import { HtmlAttrs } from "./Attrs.js";

const getAttrTests = (t) => {
  const htmlAttrs = new HtmlAttrs([{ name: "is", value: "p" }]);

  let actual = htmlAttrs.getAttr("is").name;
  let expected = "is";
  t.equal(actual, expected);

  actual = htmlAttrs.getAttr("class")?.value;
  expected = undefined;
  t.equal(actual, expected);

  t.end();
};

const hasAttrTests = (t) => {
  const htmlAttrs = new HtmlAttrs([{ name: "is", value: "p" }]);

  let actual = htmlAttrs.hasAttr("is");
  let expected = true;
  t.equal(actual, expected);

  actual = htmlAttrs.hasAttr("class");
  expected = false;
  t.equal(actual, expected);

  t.end();
};

const removeAttrTests = (t) => {
  const htmlAttrs = new HtmlAttrs([{ name: "is", value: "p" }]);

  let actual = htmlAttrs.length;
  let expected = 1;
  t.equal(actual, expected);

  htmlAttrs.removeAttr("is");
  actual = htmlAttrs.length;
  expected = 0;
  t.equal(actual, expected);

  t.end();
};

const setAttrTests = (t) => {
  const htmlAttrs = new HtmlAttrs([{ name: "is", value: "p" }]);

  let actual = htmlAttrs[0].value;
  let expected = "p";
  t.equal(actual, expected);

  htmlAttrs.setAttr("is", "a");
  actual = htmlAttrs[0].value;
  expected = "a";
  t.equal(actual, expected);

  t.end();
};

test("HtmlAttrs.prototype.getAttr", getAttrTests);
test("HtmlAttrs.prototype.hasAttr", hasAttrTests);
test("HtmlAttrs.prototype.removeAttr", removeAttrTests);
test("HtmlAttrs.prototype.setAttr", setAttrTests);
