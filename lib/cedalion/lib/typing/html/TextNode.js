import { ObjectModel } from "objectmodel";
import { Location } from "./Location.js";
import { ParentNode, initParentNode } from "./ParentNode.js";

initParentNode();

export const TextNode = ObjectModel({
  nodeName: "#comment",
  parentNode: ParentNode,
  sourceCodeLocation: [Location, null, undefined],
  value: String,
}).defaultTo({
  nodeName: "#comment",
});
