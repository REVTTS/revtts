import { ObjectModel } from "objectmodel";
import { ChildNodes } from "./ChildNodes.js";
import { ElementLocation } from "./ElementLocation.js";
import { HtmlAttrs } from "./HtmlAttrs.js";
import { NS } from "./NS.js";
import { ParentNode, initParentNode } from "./ParentNode.js";

initParentNode();

export const Element = ObjectModel({
  attrs: HtmlAttrs,
  childNodes: ChildNodes,
  nameSpaceURI: NS,
  nodeName: String,
  parentNode: ParentNode,
  tagName: String,
  sourceCodeLocation: [ElementLocation, null, undefined],
});
