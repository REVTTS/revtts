import { ArrayModel } from "objectmodel";
import { ChildNode, initChildNode } from "./ChildNode.js";

initChildNode();

export const ChildNodes = ArrayModel(ChildNode);
