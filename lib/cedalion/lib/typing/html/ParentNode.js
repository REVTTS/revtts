import { BasicModel } from "objectmodel";
import { Document } from "./Document.js";
import { DocumentFragment } from "./DocumentFragment.js";
import { Element } from "./Element.js";
import { Template } from "./Template.js";

/** @type {BasicModel([Document, DocumentFragment, Element, Template])} */
let ParentNode;
const initParentNode = () => {
  if (ParentNode) return;

  ParentNode = BasicModel([Document, DocumentFragment, Element, Template]);
};

export { ParentNode, initParentNode };
