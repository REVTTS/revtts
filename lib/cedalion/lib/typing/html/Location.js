import { ObjectModel } from "objectmodel";

export const Location = ObjectModel({
  startLine: Number,
  startCol: Number,
  startOffset: Number,
  endLine: Number,
  endCol: Number,
  endOffset: Number,
});
