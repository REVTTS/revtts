import { ObjectModel } from "objectmodel";
import { Location } from "./Location.js";
import { ParentNode, initParentNode } from "./ParentNode.js";

initParentNode();

export const CommentNode = ObjectModel({
  data: String,
  nodeName: "#comment",
  parentNode: ParentNode,
  sourceCodeLocation: [Location, null, undefined],
}).defaultTo({
  nodeName: "#comment",
});
