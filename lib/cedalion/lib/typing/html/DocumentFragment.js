import { ObjectModel } from "objectmodel";
import { Location } from "./Location.js";
import { ChildNodes } from "./ChildNodes.js";

export const DocumentFragment = ObjectModel({
  childNodes: ChildNodes,
  nodeName: "#document-fragment",
  sourceCodeLocation: [Location, null, undefined],
});
