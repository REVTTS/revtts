import { BasicModel } from "objectmodel";
import { CommentNode } from "./CommentNode.js";
import { DocumentType } from "./DocumentType.js";
import { Element } from "./Element.js";
import { Template } from "./Template.js";
import { TextNode } from "./TextNode.js";

/** @type {BasicModel([CommentNode, DocumentType, Element, Template, TextNode])} */
let ChildNode;
const initChildNode = () => {
  if (ChildNode) return;

  ChildNode = BasicModel([
    CommentNode,
    DocumentType,
    Element,
    Template,
    TextNode,
  ]);
};

export { ChildNode, initChildNode };
