import { BasicModel } from "objectmodel";

export const BUNDLE_EXTERNAL = "bundle-external";
export const BUNDLE_INLINE = "bundle-inline";
export const BUNDLE_INTERNAL = "bundle-internal";

export const BundleType = BasicModel([
  BUNDLE_EXTERNAL,
  BUNDLE_INLINE,
  BUNDLE_INTERNAL,
]).as("BundleType");
