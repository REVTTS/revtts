import { FunctionModel, ObjectModel } from "objectmodel";
import { BundleType } from "./BundleType.js";

export const FileObjectFromParams = ObjectModel({
  bundleType: BundleType,
  contents: Buffer,
  path: String,
  text: String,
});

export const FileObjectFrom =
  FunctionModel(FileObjectFromParams).as("FileObject.from");
