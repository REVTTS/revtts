import { parse } from "node:path";
import { ObjectModel } from "objectmodel";
import { hashBuffer } from "../../3_bundle/lib/encoding/hashBuffer.js";
import { getMimeType } from "../../mime/getType.js";
import { BundleType } from "./BundleType.js";
import { FileObjectFrom } from "./From.js";

export class FileObject extends ObjectModel({
  bundleType: BundleType,
  contents: Buffer,
  hash: String,
  mimeType: String,
  path: String,
  pathVersioned: String,
  text: String,
}).as("FileObject") {
  /** @type {(properties: FileObjectFromParams) => FileObject} */
  static from = new FileObjectFrom((properties) => {
    properties.hash = hashBuffer(properties.contents);
    properties.mimeType = getMimeType(properties.path);

    const { dir, ext, name } = parse(properties.path);
    properties.pathVersioned = `${dir}/${name}.${properties.hash}${ext}`;

    return new FileObject(properties);
  });
}

/** @typedef {import('./From.js').FileObjectFromParams} FileObjectFromParams */
