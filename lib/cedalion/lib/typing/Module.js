import { BasicModel, FunctionModel, ObjectModel } from "objectmodel";
import { PromiseModel } from "./PromiseModel.js";
import { WebComponent } from "./WebComponent/WebComponent.js";

export const CedalionModuleSyncFn = FunctionModel(WebComponent)
  .return(WebComponent)
  .as("CedalionModuleFn");

export const CedalionModulePromiseFn = FunctionModel(WebComponent).return(
  PromiseModel(WebComponent),
);

export const CedalionModuleFn = BasicModel([
  CedalionModuleSyncFn,
  CedalionModulePromiseFn,
]);

export const CedalionModule = ObjectModel({
  onLoad: CedalionModuleFn,
  onCompose: CedalionModuleFn,
  onBundle: CedalionModuleFn,
  onFinalize: CedalionModuleFn,
  onSave: CedalionModuleFn,
}).as("CedalionModule");
