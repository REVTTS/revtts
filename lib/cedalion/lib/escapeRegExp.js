/**
 * @param {string} stringToGoIntoTheRegex
 * @returns {string}
 */
export const escapeRegExp = (stringToGoIntoTheRegex) => {
  return stringToGoIntoTheRegex.replace(/[-/\\^$*+?.()|[\]{}]/g, "\\$&");
};
