import { composeComponentHtml } from "./lib/composeComponentHtml/composeComponentHtml.js";
import { ComposedComponent } from "./lib/ComposedComponent.js";
import { dependencySortFn } from "./lib/dependencySortFn.js";
import { generateHtml } from "../ast/html/generate.js";
import { uniqueFilterFn } from "../uniqueFilterFn.js";
import { parseHtml } from "../ast/html/parse.js";

/**
 * @param {Array<LoadedComponent>} loadedComponents
 * @returns {Array<ComposedComponent>}
 */
export const composeComponents = (loadedComponents) => {
  const dependencySortedComponents = loadedComponents.sort(dependencySortFn);

  /** @type {Map<string, LoadedComponent>} */
  const composedComponentMap = new Map();
  dependencySortedComponents.forEach((component) => {
    const htmlAst = parseHtml(component.html);
    const compiledNodes = composeComponentHtml(htmlAst, composedComponentMap);
    htmlAst.childNodes = compiledNodes;
    const html = generateHtml(htmlAst);

    const otherFileDependencies = getOtherDependencies(
      component,
      composedComponentMap,
    );
    const fileDependencies = []
      .concat(
        component.bundleForRenderFileDependencyPaths,
        otherFileDependencies,
      )
      .filter(uniqueFilterFn);

    composedComponentMap.set(
      component.name,
      new ComposedComponent(
        html,
        component.name,
        component.path,
        fileDependencies,
      ),
    );
  });

  return Array.from(composedComponentMap.values());
};

/**
 * @param {LoadedComponent} component
 * @param {Map<string, ComposedComponent>} componentMap
 * @returns {Array<string>}
 */
const getOtherDependencies = (component, componentMap) => {
  return component.componentDependencies
    .map(
      (componentDependency) =>
        componentMap.get(componentDependency)
          ?.bundleForRenderFileDependencyPaths || [],
    )
    .reduce((prev, curr) => prev.concat(curr), []);
};

/** @typedef {import('../load/lib/LoadedComponent/LoadedComponent.js').LoadedComponent} LoadedComponent */
