export class ComposedComponent {
  /** @type {string} */
  html;
  /** @type {string} */
  name;
  /** @type {string} */
  path;

  /** @type {Array<string>} */
  bundleForRenderFileDependencyPaths;

  /**
   * @param {string} html
   * @param {string} name
   * @param {string} path
   * @param {Array<string>} bundleForRenderFileDependencyPaths
   */
  constructor(html, name, path, bundleForRenderFileDependencyPaths) {
    this.html = html;
    this.name = name;
    this.path = path;

    this.bundleForRenderFileDependencyPaths =
      bundleForRenderFileDependencyPaths;
  }
}
