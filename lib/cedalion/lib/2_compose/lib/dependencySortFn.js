/**
 * @param {LoadedComponent} a
 * @param {LoadedComponent} b
 * @returns {number}
 */
export const dependencySortFn = (a, b) => {
  const aIsDependencyOfB = b.componentDependencies.includes(a.name);
  const bIsDependencyOfA = a.componentDependencies.includes(b.name);

  if (aIsDependencyOfB && bIsDependencyOfA) {
    throw new Error(
      `Component ${a.name} and ${b.name} are recursively dependent on eachother!`,
    );
  }

  if (aIsDependencyOfB) {
    return -1;
  }

  if (bIsDependencyOfA) {
    return 1;
  }

  return a.componentDependencies.length - b.componentDependencies.length;
};

/** @typedef {import('../../1_load/lib/LoadedComponent/LoadedComponent.js').LoadedComponent} LoadedComponent */
