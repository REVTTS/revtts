export class WebComponentSlotPosition {
  index;
  parentNode;
  slotName;

  /**
   * @param {number} index
   * @param {Node} parentNode
   * @param {string} slotName
   */
  constructor(index, parentNode, slotName) {
    this.index = index;
    this.parentNode = parentNode;
    this.slotName = slotName;
  }
}

/** @typedef {import('../../../../ast/html/types.js').Node} Node */
