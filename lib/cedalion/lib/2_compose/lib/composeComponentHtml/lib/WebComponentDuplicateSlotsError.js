export class WebComponentDuplicateSlotsError extends Error {
  /**
   * @param {string} tagName
   * @param {string} slotName
   */
  constructor(tagName, slotName) {
    super(`Component: "${tagName}" has multiple slots for "${slotName}"`);
    this.name = "WebComponentDuplicateSlotsError";
  }
}
