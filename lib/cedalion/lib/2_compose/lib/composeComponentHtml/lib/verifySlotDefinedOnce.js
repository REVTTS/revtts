import { WebComponentDuplicateSlotsError } from "./WebComponentDuplicateSlotsError.js";

/**
 * @param {string} tagName
 * @param {Array<WebComponentSlotPosition>} slotPositions
 */
export const verifySlotDefinedOnce = (tagName, slotPositions) => {
  const slotSet = new Set();

  slotPositions.forEach((slotPosition) => {
    const slotName = slotPosition.slotName;

    if (slotSet.has(slotPosition[0])) {
      throw new WebComponentDuplicateSlotsError(tagName, slotName);
    } else {
      slotSet.add(slotName);
    }
  });
};

/** @typedef {import('./WebComponentSlotPosition.js').WebComponentSlotPosition} WebComponentSlotPosition */
