import { getAttrValue } from "../../../../ast/html/getAttr.js";
import { getNodesByTagName } from "../../../../ast/html/getNodesByTagName.js";

import { WebComponentSlotPosition } from "./WebComponentSlotPosition.js";

/**
 * @param {Array<Node>} componentNodes
 * @returns {Array<Node>}
 */
export const findSlotPositions = (componentNodes) => {
  const slotPositions = [];
  const slotNodes = componentNodes
    .map((componentNode) => getNodesByTagName(componentNode, "slot"))
    .reduce((arrayOne, arrayTwo) => arrayOne.concat(arrayTwo), []);

  for (const slotNode of slotNodes) {
    const slotName = getAttrValue(slotNode, "name") || "default";

    slotPositions.push(
      new WebComponentSlotPosition(
        slotNode.parentNode.childNodes.indexOf(slotNode),
        slotNode.parentNode,
        slotName,
      ),
    );
  }

  slotPositions.sort((a, b) => b.index - a.index);
  return slotPositions;
};

/** @typedef {import('../../../../ast/html/types.js').Node} Node */
