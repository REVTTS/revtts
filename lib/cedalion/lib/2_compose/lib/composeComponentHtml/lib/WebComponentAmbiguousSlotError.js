export class WebComponentAmbiguousSlotError extends Error {
  /**
   * @param {string} tagName
   */
  constructor(tagName) {
    super(
      `Component: "${tagName}"'s slot position is ambiguous. A slot must ` +
        "be defined to determine where to add children of this component",
    );
    this.name = "WebComponentAmbiguousSlotError";
  }
}
