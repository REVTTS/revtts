import { WebComponentAmbiguousSlotError } from "./WebComponentAmbiguousSlotError.js";
import { verifySlotDefinedOnce } from "./verifySlotDefinedOnce.js";
import { findSlotPositions } from "./findSlotPositions.js";
import { mapChildNodesToSlot } from "./mapChildNodesToSlot.js";

/**
 * @param {string} tagName
 * @param {Array<Node>} componentNodes
 * @param {Array<Node>} childNodes
 */
export const applyNodesToComponentSlots = (
  tagName,
  componentNodes,
  childNodes,
) => {
  const isNothingToDo = componentNodes.length === 0;
  if (isNothingToDo) {
    return;
  }

  const isThereChildrenToAdd = childNodes.length === 0;
  if (isThereChildrenToAdd) {
    return;
  }

  const isOneWebComponentNode = componentNodes.length === 1;
  const isWebComponentEmpty = componentNodes[0].childNodes.length === 0;
  if (isOneWebComponentNode && isWebComponentEmpty) {
    componentNodes[0].childNodes = childNodes;
    return;
  }

  const slotPositions = findSlotPositions(componentNodes);
  const isSlotPositionAmbiguous = slotPositions.length === 0;
  if (isSlotPositionAmbiguous) {
    throw new WebComponentAmbiguousSlotError(tagName);
  }
  verifySlotDefinedOnce(tagName, slotPositions);

  const slotToChildNode = mapChildNodesToSlot(childNodes);

  slotPositions.forEach((slotPosition) => {
    const slotName = slotPosition.slotName;
    const slotParent = slotPosition.parentNode;
    const slotIndex = slotPosition.index;

    const childrenToSlot = slotToChildNode.get(slotName);
    if (childrenToSlot) {
      slotParent.childNodes.splice(slotIndex, 1, ...childrenToSlot);
    }
  });
};

/** @typedef {import('../../../../ast/html/types.js').Node} Node */
