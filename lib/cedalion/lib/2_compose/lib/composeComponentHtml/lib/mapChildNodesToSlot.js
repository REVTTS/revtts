import { getAttrValue } from "../../../../ast/html/getAttr.js";

/**
 * @param {Array<Node>} childNodes
 * @returns {Map<string, Node>}
 */
export const mapChildNodesToSlot = (childNodes) => {
  const slotToChildNode = new Map();

  childNodes.forEach((node) => {
    const slotName = getAttrValue(node, "slot") || "default";

    if (slotToChildNode.has(slotName) === false) {
      slotToChildNode.set(slotName, []);
    }

    slotToChildNode.get(slotName).push(node);
  });

  return slotToChildNode;
};

/** @typedef {import('../../../../ast/html/types.js').Node} Node */
