import { parseHtml } from "../../../ast/html/parse.js";
import { isHtmlElementTagName } from "../../../ast/html/isHtmlElementTagName.js";
import { applyNodesToComponentSlots } from "./lib/applyNodesToComponentSlots.js";

/**
 * @param {Node} htmlAst
 * @param {Map<string, ComposedComponent>} webComponentMap
 * @returns {Array<Node>}
 */
export const composeComponentHtml = (htmlAst, webComponentMap) => {
  const nodes = [];

  if (htmlAst.childNodes === undefined) {
    return [htmlAst];
  }

  if (htmlAst.nodeName === "#document") {
    for (let i = 0; i < htmlAst.childNodes.length; i++) {
      if (htmlAst.childNodes[i].nodeName === "html") {
        htmlAst.childNodes[i].childNodes = composeComponentHtml(
          htmlAst.childNodes[i],
          webComponentMap,
        );
        break;
      }
    }

    return htmlAst.childNodes;
  }

  for (let i = htmlAst.childNodes.length - 1; i >= 0; i--) {
    const childNode = htmlAst.childNodes[i];
    const tagName = childNode.tagName;
    const childNodeChildren = composeComponentHtml(childNode, webComponentMap);

    if (webComponentMap.has(tagName)) {
      const componentHtml = webComponentMap.get(tagName).html;
      const componentAst = parseHtml(componentHtml);
      if (componentAst.childNodes.length === 1) {
        componentAst.childNodes[0].attrs =
          componentAst.childNodes[0].attrs.concat(childNode.attrs);
      }
      const componentNodes = composeComponentHtml(
        componentAst,
        webComponentMap,
      );
      applyNodesToComponentSlots(tagName, componentNodes, childNodeChildren);
      nodes.unshift(...componentNodes);
    } else if (childNode.nodeName === "#text") {
      childNode.value = childNode.value.trim();
      nodes.unshift(childNode);
    } else {
      childNode.childNodes = childNodeChildren;
      nodes.unshift(childNode);
    }
  }

  return nodes;
};

/** @typedef {import('../ComposedComponent.js').ComposedComponent} ComposedComponent */
/** @typedef {import('../../../ast/html/types.js').Node} Node */
