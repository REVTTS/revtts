import { dirname, resolve } from "node:path";
import { fileURLToPath } from "node:url";

/**
 * @param {FinalizedComponent} finalizedComponent
 * @param {string} inDir
 * @param {string} outDir
 * @param {Object} dependencies
 * @param {Object} dependencies.mkdir
 * @param {writeFile} dependencies.writeFile
 */
export const saveComponent = (finalizedComponent, outDir, dependencies) => {
  const { mkdir, writeFile } = dependencies;

  return Promise.all([
    doWriteFile(
      resolve(outDir, `./${finalizedComponent.name}.html`),
      finalizedComponent.html,
      mkdir,
      writeFile,
    ),
    ...finalizedComponent.files.map((fileObj) =>
      doWriteFile(
        resolve(outDir, `.${fileObj.path}`),
        fileObj.contents,
        mkdir,
        writeFile,
      ),
    ),
  ]);
};

/** @type {Map<string, Promise>} */
const fileToPromiseMap = new Map();

/**
 * @param {path} string
 * @param {Buffer | string} content
 * @param {mkdir} mkdir
 * @param {writeFile} writeFile
 * @returns {Promise<void>}
 */
const doWriteFile = (path, content, mkdir, writeFile) => {
  if (fileToPromiseMap.has(path)) {
    return fileToPromiseMap.get(fileURLToPath);
  }

  const promise = new Promise((resolve, reject) => {
    mkdir(dirname(path), { recursive: true }, (dirError) => {
      if (dirError) {
        reject(dirError);
        return;
      }

      writeFile(path, content, "utf-8", (error) => {
        if (error) {
          reject(error);
          return;
        }
        resolve();
      });
    });
  });

  fileToPromiseMap.set(path, promise);

  return promise;
};

/** @typedef {import('../4_finalize/lib/FinalizedComponent.js').FinalizedComponent} FinalizedComponent */
/** @typedef {import('../models/FileObject/FileObject.js'). FileObject} FileObject */
/** @typedef {import('node:fs').mkdir} mkdir */
/** @typedef {import('node:fs').writeFile} writeFile */
