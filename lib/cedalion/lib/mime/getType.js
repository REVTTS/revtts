import mime from "mime";

export const getMimeType = mime.getType;
