import { parse, resolve } from "node:path";

import { parseHtml } from "../../../ast/html/parse.js";
import { generateHtml } from "../../../ast/html/generate.js";
import { walkHtml } from "../../../ast/html/walk.js";
import { hasAttr } from "../../../ast/html/hasAttr.js";
import { getAttrValue } from "../../../ast/html/getAttr.js";
import { setAttrValue } from "../../../ast/html/setAttr.js";
import { isHtmlElementTagName } from "../../../ast/html/isHtmlElementTagName.js";
import { removeAttr } from "../../../ast/html/removeAttr.js";

export class LoadedComponent {
  /** @type {string} */
  html;
  /** @type {string} */
  name;
  /** @type {string} */
  path;

  /** @type {Array<string>} */
  componentDependencies;

  /** @type {Array<string>} */
  bundleForRenderFileDependencyPaths;

  /**
   * @param {string} html
   * @param {string} path
   */
  constructor(html, path) {
    this.path = resolve(path);
    const parsedPath = parse(this.path);

    this.name = parsedPath.name;
    this.componentDependencies = [];

    this.bundleForRenderFileDependencyPaths = [];

    let ast = parseHtml(html);
    this.#populateCssBundleForRenderDependences(ast, parsedPath.dir);
    this.#populateIconBundleForRenderDependences(ast, parsedPath.dir);
    this.#populateJsBundleForRenderDependences(ast, parsedPath.dir);
    this.#populateSvgBundleForRenderDependences(ast, parsedPath.dir);
    ast = this.#removeBundleForRenderDependencies(ast);

    this.#populateComponentDependencies(ast);
    const newHtml = generateHtml(ast);
    this.html = newHtml.trim();
  }

  /** @param {Node} node */
  #populateComponentDependencies(node) {
    const componentDependencySet = new Set();

    walkHtml(node, (childNode, _, __) => {
      if (childNode.tagName === undefined) {
        return;
      }

      const isComponentDependency =
        isHtmlElementTagName(childNode.tagName) === false;
      if (isComponentDependency) {
        componentDependencySet.add(childNode.tagName);
      }
    });

    componentDependencySet.forEach((value) => {
      this.componentDependencies.push(value);
    });
  }

  /**
   * @param {Node} node
   * @param {string} dir
   */
  #populateCssBundleForRenderDependences(node, dir) {
    const cssDependencySet = new Set();

    walkHtml(node, (childNode, _, __) => {
      if (isBundleForRender(childNode) === false) {
        return;
      }

      if (isLinkTag(childNode) === false) {
        return;
      }

      if (isStyleSheetRel(childNode) === false) {
        return;
      }

      const href = getAttrValue(childNode, "href");
      const path = resolve(dir, href);
      cssDependencySet.add(path);
    });

    this.bundleForRenderFileDependencyPaths.push(...cssDependencySet.values());
  }

  /**
   * @param {Node} node
   * @param {string} dir
   */
  #populateIconBundleForRenderDependences(node, dir) {
    const iconDependencySet = new Set();

    walkHtml(node, (childNode, _, __) => {
      if (isBundleForRender(childNode) === false) {
        return;
      }

      if (isIconRel(childNode) === false) {
        return;
      }

      const href = getAttrValue(childNode, "href");
      const path = resolve(dir, href);
      iconDependencySet.add(path);
    });

    this.bundleForRenderFileDependencyPaths.push(...iconDependencySet.values());
  }

  /**
   * @param {Node} node
   * @param {string} dir
   */
  #populateSvgBundleForRenderDependences(node, dir) {
    const svgDependencySet = new Set();

    walkHtml(node, (childNode, _, __) => {
      if (isBundleForRender(childNode) === false) {
        return;
      }

      if (isUseTag(childNode) === false) {
        return;
      }

      removeAttr(childNode, "bundle-for-render");

      const href = getAttrValue(childNode, "href");
      const path = resolve(dir, href);
      const parsedPath = parse(path);
      setAttrValue(childNode, "href", `#icon-${parsedPath.name}`);
      svgDependencySet.add(path);
    });

    this.bundleForRenderFileDependencyPaths.push(...svgDependencySet.values());
  }

  /**
   * @param {Node} node
   * @param {string} dir
   */
  #populateJsBundleForRenderDependences(node, dir) {
    const jsDependencySet = new Set();

    walkHtml(node, (childNode, _, __) => {
      if (isBundleForRender(childNode) === false) {
        return;
      }

      if (isScriptTag(childNode) === false) {
        return;
      }

      const src = getAttrValue(childNode, "src");
      const path = resolve(dir, src);
      jsDependencySet.add(path);
    });

    this.bundleForRenderFileDependencyPaths.push(...jsDependencySet.values());
  }

  /** @param {Node} node */
  #removeBundleForRenderDependencies(node) {
    walkHtml(node, (childNode, index, parentArray) => {
      if (isBundleForRender(childNode) === false) {
        return;
      }

      parentArray.splice(index, 1);
    });

    return node;
  }
}

/** @param {Node} node */
const isBundleForRender = (node) => hasAttr(node, "bundle-for-render");

/** @param {Node} node */
const isLinkTag = (node) => node.tagName === "link";

/** @param {Node} node */
const isUseTag = (node) => node.tagName === "use";

/** @param {Node} node */
const isStyleSheetRel = (node) => getAttrValue(node, "rel") === "stylesheet";

/** @param {Node} node */
const isIconRel = (node) => getAttrValue(node, "rel") === "icon";

/** @param {Node} node */
const isScriptTag = (node) => node.tagName === "script";

/** @typedef {import('../../../ast/html/types.d.ts').Node} Node */
