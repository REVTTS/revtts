import { LoadedComponent } from "./LoadedComponent/LoadedComponent.js";
import { uniqueFilterFn } from "../../uniqueFilterFn.js";

/**
 * @param {Array<string>} filePaths
 * @param {readFile} readFile
 *
 * @returns {Promise<Array<LoadedComponent>>}
 */
export const componentPathsToLoadedComponents = (filePaths, readFile) => {
  return new Promise((resolve, reject) => {
    const loadedComponents = [];

    filePaths.filter(uniqueFilterFn).forEach((filePath, index, array) => {
      readFile(filePath, { encoding: "utf8" }, (err, data) => {
        if (err) {
          reject(err);
          return;
        }

        loadedComponents.push(new LoadedComponent(data, filePath));

        if (loadedComponents.length === array.length) {
          resolve(loadedComponents);
        }
      });
    });
  });
};

/** @typedef {import('fs').readFile} readFile */
