import { extname, join } from "node:path";

/**
 * @param {string} componentDir
 * @param {readdir} readdir
 *
 * @returns {Promise<Array<string>>}
 */
export const getComponentPaths = (componentDir, readdir) => {
  return new Promise((resolve, reject) => {
    readdir(
      componentDir,
      { encoding: "utf8", recursive: true },
      (err, foundFiles) => {
        if (err) {
          reject(err);
          return;
        }

        const filePaths = foundFiles
          .filter((filePath) => extname(filePath) === ".html")
          .map((filePath) => join(componentDir, filePath));

        resolve(filePaths);
      },
    );
  });
};

/** @typedef {import('fs').readdir} readdir */
