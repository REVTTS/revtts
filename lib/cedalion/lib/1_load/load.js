import { getComponentPaths } from "./lib/getComponentPaths.js";
import { componentPathsToLoadedComponents } from "./lib/componentPathsToLoadedComponents.js";

/**
 * @param {string} dir
 * @param {object} dependencies
 * @param {existsSync} dependencies.existsSync
 * @param {readFile} dependencies.readFile
 * @param {readdir} dependencies.readdir
 */
export const loadComponents = async (dir, dependencies) => {
  const componentPaths = await getComponentPaths(dir, dependencies.readdir);
  const loadedComponents = await componentPathsToLoadedComponents(
    componentPaths,
    dependencies.readFile,
  );

  return loadedComponents;
};

/** @typedef {import('node:fs').existsSync} existsSync */
/** @typedef {import('node:fs').readdir} readdir */
/** @typedef {import('node:fs').readFile} readFile */
