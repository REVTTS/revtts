import { parse } from "node:path";
import { hashBuffer } from "./encoding/hashBuffer.js";
import { FileObject } from "../../FileObject.js";

/**
 * @param {ComposedComponent} composedComponent
 * @param {readFile} readFile
 * @returns {Promise<Array<FileObject>}
 */
export const bundleComponentSvg = (composedComponent, readFile) => {
  const svgFilePaths = composedComponent.bundleForRenderFileDependencyPaths
    .filter((filePath) => filePath.endsWith(".svg"))
    .sort();

  if (svgFilePaths.length === 0) {
    return Promise.resolve([]);
  }

  const svgJointPaths = svgFilePaths.join("|");
  if (svgResultMap.has(svgJointPaths)) {
    return svgResultMap.get(svgJointPaths);
  }

  /** @type {Array<Promise<FileObject>>} */
  const svgPromises = [];
  for (const svgFilePath of svgFilePaths) {
    if (svgLoadedFileMap.has(svgFilePath)) {
      svgPromises.push(svgLoadedFileMap.get(svgFilePath));
      continue;
    }

    /** @type {Promise<FileObject>} */
    const svgPromise = new Promise((resolve, reject) => {
      readFile(svgFilePath, async (err, fileBuffer) => {
        if (err) {
          reject(err);
          return;
        }
        const parsedPath = parse(svgFilePath);

        resolve(
          new FileObject(
            new Uint8Array(fileBuffer.buffer),
            hashBuffer(fileBuffer),
            `/svgs/${parsedPath.base}`,
            fileBuffer.toString("utf-8"),
          ),
        );
      });
    });

    svgPromises.push(svgPromise);
    svgLoadedFileMap.set(svgFilePath, svgPromise);
  }

  const promiseAll = Promise.all(svgPromises).then((svgFileObjects) => {
    svgFileObjects.sort(sortFileObjects);
    const svgTexts = svgFileObjects.map((fileObject) => {
      const parsedPath = parse(fileObject.path);

      let svgText = fileObject.text.replaceAll(/\s+/g, " ");
      svgText = svgText.replace(/<\/svg>/, "</symbol>");
      const viewport = /(viewBox=".+?")/.exec(fileObject.text)[1] || "";
      svgText = svgText.replace(
        /<svg.+?>/,
        `<symbol id="svg-${parsedPath.name}" ${viewport}>`,
      );

      return svgText;
    });

    const bundledSvgText = svgTexts.join("");
    const buffer = Buffer.from(bundledSvgText, "utf-8");
    const hash = hashBuffer(buffer);

    return [
      new FileObject(
        new Uint8Array(buffer.buffer),
        hash,
        `/svg/${hash}.svg`,
        bundledSvgText,
      ),
    ];
  });
  svgResultMap.set(svgJointPaths, promiseAll);

  return promiseAll;
};

/** @type {Map<string, Array<FileObject>>} */
const svgResultMap = new Map();

/** @type {Map<string, Buffer>} */
const svgLoadedFileMap = new Map();

const sortFileObjects = (a, b) => {
  if (a.path < b.path) {
    return -1;
  }

  if (a.path > b.path) {
    return 1;
  }

  return 0;
};

/** @typedef {import('node:fs').readFile} readFile */
/** @typedef {import('../../2_compose/lib/ComposedComponent.js').ComposedComponent} ComposedComponent */
