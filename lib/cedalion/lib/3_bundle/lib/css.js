import * as esbuild from "esbuild";
import { parse } from "node:path";
import { FileObject } from "../../FileObject.js";
import { reEncodeString } from "./encoding/reEncodeString.js";

/**
 * @param {ComposedComponent} composedComponent
 * @returns {Promise<Array<FileObject>>}
 */
export const bundleComponentCss = async (composedComponent) => {
  const cssFilePaths =
    composedComponent.bundleForRenderFileDependencyPaths.filter((filePath) =>
      filePath.endsWith(".css"),
    );
  if (cssFilePaths.length === 0) {
    return [];
  }

  const cssImportString = cssFilePaths
    .sort()
    .map(
      (cssBundletimeDependency) => `@import url("${cssBundletimeDependency}");`,
    )
    .join("\n");

  if (cssResultMap.has(cssImportString)) {
    return await cssResultMap.get(cssImportString);
  }

  const cssBuildResultPromise = esbuild
    .build({
      stdin: {
        contents: cssImportString,
        loader: "css",
        resolveDir: "/",
      },
      bundle: true,
      loader: { ".ttf": "copy" },
      minify: false,
      outdir: ".",
      treeShaking: true,
      write: false,
    })
    .then((cssEsbuildResult) => {
      if (cssEsbuildResult.warnings.length > 0) {
        console.error(cssEsbuildResult.warnings);
      }

      const cssFiles = [];
      const fontFiles = [];

      const esBuildCssFiles = cssEsbuildResult.outputFiles.filter((file) =>
        file.path.endsWith(".css"),
      );
      const esBuildFontFiles = cssEsbuildResult.outputFiles.filter((file) =>
        file.path.endsWith(".ttf"),
      );

      for (const cssFile of esBuildCssFiles) {
        const parsedPath = parse(composedComponent.path);

        cssFiles.push(
          new FileObject(
            cssFile.contents,
            reEncodeString(cssFile.hash, "base64", "base64url"),
            `/css/${parsedPath.name}.css`,
            cssFile.text.replace("/* <stdin> */", "").trim(),
          ),
        );
      }

      for (const fontFile of esBuildFontFiles) {
        const parsedPath = parse(fontFile.path);
        const splitPathName = parsedPath.name.split("-");
        const newFilePath = `/fonts/${splitPathName[0]}${parsedPath.ext}`;

        for (const cssFile of cssFiles) {
          const fontPathIsInCss = cssFile.text.indexOf(parsedPath.base) !== -1;
          if (fontPathIsInCss === false) {
            continue;
          }

          // This is meant to be regex. So the escape is used.
          // eslint-disable-next-line no-useless-escape
          const reReplace = new RegExp(
            String.raw`url\(".*?${parsedPath.base}"\)`,
            "g",
          );
          cssFile.text = cssFile.text.replaceAll(
            reReplace,
            `url(${newFilePath})`,
          );
        }

        fontFiles.push(
          new FileObject(
            fontFile.contents,
            reEncodeString(fontFile.hash, "base64", "base64url"),
            newFilePath,
            "",
          ),
        );
      }

      return [...cssFiles, ...fontFiles];
    });

  cssResultMap.set(cssImportString, cssBuildResultPromise);

  return await cssBuildResultPromise;
};

/** @type {Map<string, Promise<FileObject>>} */
const cssResultMap = new Map();

/** @typedef {import('../../2_compose/lib/ComposedComponent.js').ComposedComponent} ComposedComponent */
