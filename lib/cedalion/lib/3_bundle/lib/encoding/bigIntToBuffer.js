import { reEncodeString } from "./reEncodeString.js";

export const bigIntToBuffer = (num) => {
  const hex = num.toString(16);
  return reEncodeString(hex, "hex", "base64url");
};
