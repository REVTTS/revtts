import { XXH64 } from "xxh3-ts";
import { bigIntToBuffer } from "./bigIntToBuffer.js";

/**
 * @param {Buffer} buffer
 * @returns {string}
 */
export const hashBuffer = (buffer) => {
  const hexInt = XXH64(buffer);
  return bigIntToBuffer(hexInt);
};
