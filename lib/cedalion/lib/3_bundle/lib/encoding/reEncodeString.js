export const reEncodeString = (str, oldEncoding, newEncoding) => {
  // Padding *is* needed otherwise the last nibble will be dropped in an edge case
  return Buffer.from(
    str.padStart(Math.ceil(str.length / 2) * 2, "0"),
    oldEncoding,
  ).toString(newEncoding);
};
