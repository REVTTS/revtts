import * as esbuild from "esbuild";
import { parse } from "node:path";
import { FileObject } from "../../FileObject.js";
import { reEncodeString } from "./encoding/reEncodeString.js";

/**
 * @param {ComposedComponent} composedComponent
 * @returns {Promise<Array<FileObject>>}
 */
export const bundleComponentScript = async (composedComponent) => {
  const jsFilePaths =
    composedComponent.bundleForRenderFileDependencyPaths.filter((filePath) =>
      filePath.endsWith(".js"),
    );

  if (jsFilePaths.length === 0) {
    return [];
  }

  const jsImportString = jsFilePaths
    .sort()
    .map((jsBundletimeDependency) => `import "${jsBundletimeDependency}";`)
    .join("\n");

  if (jsResultMap.has(jsImportString)) {
    return await jsResultMap.get(jsImportString);
  }

  const jsEsbuildPromise = esbuild
    .build({
      stdin: {
        contents: jsImportString,
        loader: "ts",
        resolveDir: "/",
      },
      bundle: true,
      format: "iife",
      minify: false,
      treeShaking: true,
      write: false,
    })
    .then((jsEsbuildResult) => {
      if (jsEsbuildResult.warnings.length > 0) {
        throw new Error(jsEsbuildResult.warnings);
      }

      if (jsEsbuildResult.outputFiles.length > 1) {
        throw new Error(
          `More than one js output for ${composedComponent.name}`,
        );
      }
      const jsFiles = [];

      for (const jsFile of jsEsbuildResult.outputFiles) {
        const parsedPath = parse(composedComponent.path);

        jsFiles.push(
          new FileObject(
            jsFile.contents,
            reEncodeString(jsFile.hash, "base64", "base64url"),
            `/js/${parsedPath.name}.js`,
            jsFile.text.replace("/* <stdin> */", "").trim(),
          ),
        );
      }

      return jsFiles;
    });
  jsResultMap.set(jsImportString, jsEsbuildPromise);

  return await jsEsbuildPromise;
};

/** @type {Map<string, Promise<Array<FileObject>>>} */
const jsResultMap = new Map();

/** @typedef {import('../../2_compose/lib/ComposedComponent').ComposedComponent} ComposedComponent */
