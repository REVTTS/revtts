export class BundledComponent {
  /** @type {Map<string, FileObject>} */
  static fileHashMap;

  static {
    this.fileHashMap = new Map();
  }

  /** @type {string} */
  name;
  /** @type {string} */
  path;
  /** @type {string} */
  html;
  /** @type {Array<FileObject>} */
  files;

  /**
   * @param {string} name
   * @param {string} path
   *
   * @param {string} html
   *
   * @param {Array<FileObject>} files
   */
  constructor(name, path, html, files) {
    this.name = name;
    this.path = path;

    this.html = html;
    this.files = [];

    const fileMap = new Map();
    this.#handleFile(fileMap, files);

    this.files.push(...fileMap.values());
  }

  /**
   * @param {Array<FileObject>} fileMap
   * @param {Array<FileObject>} staticFilesMap
   * @returns {void}
   */
  #handleFile(fileMap, files) {
    for (const file of files) {
      if (fileMap.has(file.hash)) {
        continue;
      }
      const addedFile = BundledComponent.addFile(file);
      fileMap.set(addedFile.hash, addedFile);
    }
  }

  /**
   * @param {FileObject} file
   */
  static addFile(file) {
    const existingFile = this.fileHashMap.get(file.hash);
    if (existingFile !== undefined) {
      return existingFile;
    }

    if (fileNameSet.has(file.path)) {
      throw new Error(
        `Multiple files, with different hashes, resolve to ${file.path}. Rename one or both files.`,
      );
    }

    this.fileHashMap.set(file.hash, file);
    fileNameSet.add(file.path);

    return file;
  }
}

const fileNameSet = new Set();

/** @typedef {import('../../models/FileObject/FileObject.js').FileObject} FileObject */
