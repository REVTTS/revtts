import { parse } from "node:path";
import { hashBuffer } from "./encoding/hashBuffer.js";
import { FileObject } from "../../FileObject.js";

/**
 * @param {ComposedComponent} composedComponent
 * @param {readFile} readFile
 * @returns {Promise<Array<FileObject>}
 */
export const bundleComponentIcons = (composedComponent, readFile) => {
  const iconFilePaths =
    composedComponent.bundleForRenderFileDependencyPaths.filter((filePath) =>
      filePath.endsWith(".ico"),
    );

  const iconPromises = [];
  for (const iconDependency of iconFilePaths) {
    if (iconResultMap.has(iconDependency)) {
      iconPromises.push(iconResultMap.get(iconDependency));
      continue;
    }

    const iconPromise = new Promise((resolve, reject) => {
      readFile(iconDependency, (err, fileBuffer) => {
        if (err) {
          reject(err);
          return;
        }
        const parsedPath = parse(iconDependency);

        resolve(
          new FileObject(
            new Uint8Array(fileBuffer.buffer),
            hashBuffer(fileBuffer),
            `/icons/${parsedPath.base}`,
            "",
          ),
        );
      });
    });

    iconPromises.push(iconPromise);
    iconResultMap.set(iconDependency, iconPromise);
  }

  return Promise.all(iconPromises);
};

/** @type {Map<string, Buffer>} */
const iconResultMap = new Map();

/** @typedef {import('node:fs').readFile} readFile */
/** @typedef {import('../../2_compose/lib/ComposedComponent').ComposedComponent} ComposedComponent */
