import { BundledComponent } from "./lib/BundledComponent.js";

import { bundleComponentCss } from "./lib/css.js";
import { bundleComponentIcons } from "./lib/icons.js";
import { bundleComponentScript } from "./lib/script.js";
import { bundleComponentSvg } from "./lib/svg.js";

/**
 * @param {ComposedComponent} composedComponent
 * @param {readFile} readFile
 * @returns {Promise<BundledComponent>}
 */
export const bundleComponent = (composedComponent, readFile) => {
  if (composedComponent.bundleForRenderFileDependencyPaths.length === 0) {
    return Promise.resolve(
      new BundledComponent(
        composedComponent.name,
        composedComponent.path,
        composedComponent.html,
        [],
      ),
    );
  }

  return Promise.all([
    bundleComponentCss(composedComponent),
    bundleComponentIcons(composedComponent, readFile),
    bundleComponentScript(composedComponent),
    bundleComponentSvg(composedComponent, readFile),
  ]).then(
    ([
      bundledCssFiles,
      bundledIconFiles,
      bundledScriptFiles,
      bundledSvgFiles,
    ]) =>
      new BundledComponent(
        composedComponent.name,
        composedComponent.path,
        composedComponent.html,
        [
          ...bundledCssFiles,
          ...bundledIconFiles,
          ...bundledScriptFiles,
          ...bundledSvgFiles,
        ],
      ),
  );
};

/** @typedef {import('node:fs').readFile} readFile */
/** @typedef {import('../2_compose/lib/ComposedComponent.js').ComposedComponent} ComposedComponent */
