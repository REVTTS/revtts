import { parse, parseFragment } from "parse5";

/**
 * @param {string} html
 */
export const parseHtml = (html) => {
  if (html.indexOf("<html") === -1) {
    return parseFragment(html);
  } else {
    return parse(html);
  }
};
