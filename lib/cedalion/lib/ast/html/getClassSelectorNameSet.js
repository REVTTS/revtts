import { getAttrValue } from "./getAttr.js";
import { walkHtml } from "./walk.js";

/**
 * @param {Node} htmlAst
 * @returns {Set<string>}
 */
export const getHtmlClassSelectorNameSet = (htmlAst) => {
  const classSelectorNameSet = new Set();

  walkHtml(htmlAst, (htmlNode) => {
    if (htmlNode.attrs === undefined) {
      return;
    }

    const classNames = getAttrValue(htmlNode, "class");
    if (classNames === undefined) {
      return;
    }

    for (const className of classNames.split(" ")) {
      classSelectorNameSet.add(className);
    }
  });

  return classSelectorNameSet;
};

/** @typedef {import('./types.d.ts').Node} Node */
