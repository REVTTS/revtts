import { walkHtml } from "./walk.js";

/**
 * @param {Array<Node>} node
 * @param {string} tagName
 * @returns {Array<Node>}
 */
export const getNodesByTagName = (node, tagName) => {
  const foundNodes = [];

  walkHtml(node, (childNode) => {
    if (childNode.tagName === tagName) {
      foundNodes.push(childNode);
    }
  });

  return foundNodes;
};

/** @typedef {import('./types.js').Node} Node */
