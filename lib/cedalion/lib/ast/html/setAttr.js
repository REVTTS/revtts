import { removeAttr } from "./removeAttr.js";

/**
 * @param {Node} node
 * @param {string} attrName
 * @param {any} attrValue
 * @returns {void}
 */
export const setAttrValue = (node, attrName, attrValue) => {
  if (node.attrs === undefined) {
    return;
  }

  removeAttr(node, attrName);
  node.attrs.push({
    name: attrName,
    value: attrValue,
  });
};

/** @typedef {import('./types.js').Node} Node */
