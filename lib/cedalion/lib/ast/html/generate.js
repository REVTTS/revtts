import { serialize, serializeOuter } from "parse5";

/**
 * @param {Node} ast
 */
export const generateHtml = (ast) => {
  let html = "";

  if (ast.nodeName === "#document-fragment") {
    for (const childNode of ast.childNodes) {
      html += serializeOuter(childNode);
    }
  } else if (ast.nodeName === "#document") {
    html = serialize(ast);
  } else {
    html = serializeOuter(ast);
  }

  return html;
};

/** @typedef {import('./types.js').Node} Node */
