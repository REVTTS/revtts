export class Node {
  nodeName: string;
  tagName: string;
  attrs: Array<Attr>;
  nameSpaceURI: string;
  childNodes: Array<Node>;
  parentNode: Node;
}

export class Attr {
  name: string;
  value: string;
}
