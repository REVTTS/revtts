import { getAttrValue } from "./getAttr.js";

/**
 * @param {Node} node
 * @param {string} attrName
 * @returns {boolean}
 */
export const hasAttr = (node, attrName) => {
  const attr = getAttrValue(node, attrName);

  return attr !== undefined;
};

/** @typedef {import('./types.d.ts').Node} Node */
