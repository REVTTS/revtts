/**
 * @param {Node} htmlAst
 * @param {WalkHtmlCb} cb
 * @returns {void}
 */
export const walkHtml = (htmlAst, cb) => {
  if (htmlAst.childNodes === undefined) {
    return;
  }

  for (let i = htmlAst.childNodes.length - 1; i > -1; i--) {
    walkHtml(htmlAst.childNodes[i], cb);
    cb(htmlAst.childNodes[i], i, htmlAst.childNodes);
  }
};

/**
 * @callback WalkHtmlCb
 * @param {Node} childNode
 * @param {number} index
 * @param {Array<Node>} parentArray
 * @returns {void}
 */

/** @typedef {import('./types.js').Node} Node */
