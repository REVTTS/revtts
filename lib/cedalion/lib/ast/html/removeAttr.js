/**
 * @param {Node} node
 * @param {string} attrName
 * @returns {void}
 */
export const removeAttr = (node, attrName) => {
  if (node.attrs === undefined) {
    return;
  }

  for (let i = node.attrs.length - 1; i > -1; i--) {
    if (node.attrs[i].name === attrName) {
      node.attrs.splice(i, 1);
    }
  }
};

/** @typedef {import('./types.js').Node} Node */
