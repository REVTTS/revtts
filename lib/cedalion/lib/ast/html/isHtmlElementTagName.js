import { htmlElementSet } from "./htmlElementSet.js";

/**
 * @param {string} tagName
 * @returns {boolean}
 */
export const isHtmlElementTagName = (tagName) => htmlElementSet.has(tagName);
