/**
 * @param {Node} node
 * @param {string} attrName
 * @returns {string | boolean | undefined}
 */
export const getAttrValue = (node, attrName) => {
  let attrValue;

  if (node.attrs === undefined) {
    return attrValue;
  }

  for (const attr of node.attrs) {
    if (attr.name === attrName) {
      attrValue = attr.value || true;
    }
  }

  return attrValue;
};

/** @typedef {import('./types.js').Node} Node */
