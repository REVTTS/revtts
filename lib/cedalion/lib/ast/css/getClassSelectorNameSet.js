import { walkCss } from "./walk.js";

/**
 * @param {Object} cssAst
 *
 * @returns {Set<string>}
 */
export const getCssClassSelectorNameSet = (cssAst) => {
  /** {Set<string>} */
  const cssClassNameSet = new Set();
  walkCss(cssAst, {
    visit: "ClassSelector",
    enter(selectorNode) {
      cssClassNameSet.add(selectorNode.name);
    },
  });

  return cssClassNameSet;
};
