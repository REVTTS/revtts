import { generateHtml } from "../../ast/html/generate.js";
import { getAttrValue } from "../../ast/html/getAttr.js";
import { walkHtml } from "../../ast/html/walk.js";
import { getBodyAst } from "./getBodyAst.js";

/**
 * Templates and Slots are done together because we do not want to remove
 * slots that within templates. So we remove them while the templates are
 * removed from the AST so we can preserve them.
 *
 * @param {Node} htmlAst
 * @param {(htmlAst: Node) => Node} cb
 * @returns {Node}
 */
export const hoistTemplates = (htmlAst, cb) => {
  const templateMap = new Map();

  walkHtml(htmlAst, (childNode, i, parentArray) => {
    if (childNode.nodeName !== "template") {
      return;
    }

    const templateId = getAttrValue(childNode, "id");
    const template = generateHtml(childNode);
    const existingTemplate = templateMap.get(templateId);

    if (existingTemplate === undefined) {
      templateMap.set(templateId, template);
    } else if (template !== existingTemplate) {
      throw new Error(
        `Multiple templates with id "${templateId}" were found with different content!`,
      );
    }

    parentArray.splice(i, 1);
  });

  htmlAst = cb(htmlAst);

  const bodyAst = getBodyAst(htmlAst);
  bodyAst.childNodes.push(...templateMap.values());

  return htmlAst;
};

/** @typedef {import('../../ast/html/types.js').Node} Node */
