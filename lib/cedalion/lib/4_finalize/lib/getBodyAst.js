import { getNodesByTagName } from "../../ast/html/getNodesByTagName.js";

export const getBodyAst = (htmlAst) => {
  const bodyAsts = getNodesByTagName(htmlAst, "body");

  if (bodyAsts.length === 1) {
    return bodyAsts[0];
  }

  if (bodyAsts.length > 1) {
    throw new Error("Multiple body tags found in document!");
  }

  if (bodyAsts.length < 1) {
    throw new Error("No body tag found in document!");
  }
};
