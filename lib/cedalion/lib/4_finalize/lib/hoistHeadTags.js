import { generateHtml } from "../../ast/html/generate.js";
import { walkHtml } from "../../ast/html/walk.js";
import { getHeadAst } from "./getHeadAst.js";

/**
 * While noscript, script, and template elements can be in the head they are handled
 * separately so that other factors can affect their positioning. For instance, templates
 * aren't immediately necessary for rendering, so are appended to the end of the body. Any components
 * that use templates should trigger only from user events, so should not immediately need them.
 *
 * @param {Node} htmlAst
 * @returns {Node}
 */
export const hoistHeadTags = (htmlAst) => {
  const tagMap = new Map();

  walkHtml(htmlAst, (childNode, i, parentArray) => {
    const tagCanBeInhead = headTags.includes(childNode.nodeName);
    if (tagCanBeInhead === false) {
      return;
    }

    childNode.attrs = childNode.attrs.sort(attrSortFn);

    const html = generateHtml(childNode);
    if (tagMap.has(html) === false) {
      tagMap.add(html, childNode);
    }

    parentArray.splice(i, 1);
  });

  const headAst = getHeadAst(htmlAst);
  const newHeadChildren = Array.from(tagMap.values)
    .sort(headTagSortFn)
    .concat(headAst.childNodes);
  headAst.childNodes = newHeadChildren;

  return htmlAst;
};

const headTags = ["base", "link", "meta", "style", "title"];

const headTagsSortOrder = [
  "title",
  "meta",
  "base", // must come before other tag that could have an href (ie. link)
  "link",
  "style",
];

/**
 * @param {Node} tagA
 * @param {Node} tagB
 * @returns {number}
 */
const headTagSortFn = (tagA, tagB) => {
  const nodeNameA = tagA.nodeName;
  const nodeNameB = tagB.nodeName;

  if (nodeNameA === nodeNameB) {
    return 0;
  }

  for (const tagName of headTagsSortOrder) {
    if (nodeNameA === tagName) {
      return -1;
    }

    if (nodeNameB === tagName) {
      return 1;
    }
  }
};

/**
 * @param {Attr} attrA
 * @param {Attr} attrB
 * @returns {number}
 */
const attrSortFn = (attrA, attrB) => {
  const nameA = attrA.name;
  const nameB = attrB.name;

  if (nameA === nameB) {
    return 0;
  }

  if (nameA < nameB) {
    return -1;
  }

  if (nameA > nameB) {
    return 1;
  }

  return 0;
};

/** @typedef {import('../../ast/html/types.js').Attr} Attr */
/** @typedef {import('../../ast/html/types.js').Node} Node */
