import { walkHtml } from "../../ast/html/walk.js";

export const removeSlots = (htmlAst) => {
  walkHtml(htmlAst, (childNode, i, parentArray) => {
    if (childNode.nodeName !== "slot") {
      return;
    }

    parentArray.splice(i, 1);
  });

  return htmlAst;
};
