export const generateScript = (files) => {
  let script = "";
  const scriptFiles = files.filter(scriptFileFilter);
  for (const scriptFile of scriptFiles) {
    script += scriptFile.text;

    files.splice(files.indexOf(scriptFile), 1);
  }
  if (script !== "") {
    script = `<script>${script}</script>`;
  }

  return script;
};

const scriptFileFilter = (file) => file.mimeType === "application/javascript";
