import { getNodesByTagName } from "../../ast/html/getNodesByTagName.js";

export const getHeadAst = (htmlAst) => {
  const headAsts = getNodesByTagName(htmlAst, "head");

  if (headAsts.length === 1) {
    return headAsts[0];
  }

  if (headAsts.length > 1) {
    throw new Error("Multiple head tags found in document!");
  }

  if (headAsts.length < 1) {
    throw new Error("No head tags found in document!");
  }
};
