import { cleanCss } from "./css/cleanCss.js";

export const generateStyle = (files) => {
  let style = "";
  const cssFiles = files.filter(cssFileFilter);
  for (const cssFile of cssFiles) {
    style += cssFile.text;

    files.splice(files.indexOf(cssFile), 1);
  }
  style = cleanCss(style);

  if (style !== "") {
    style = `<style>${style}</style>`;
  }

  return style;
};

const cssFileFilter = (file) => file.mimeType === "text/css";
