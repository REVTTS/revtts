export const generateSvg = (files) => {
  let svg = "";
  const svgFiles = files.filter(svgFileFilter);
  for (const svgFile of svgFiles) {
    svg += svgFile.text;

    files.splice(files.indexOf(svgFile), 1);
  }

  if (svg !== "") {
    svg = `<svg xmlns="http://www.w3.org/2000/svg"><defs>${svg}</defs></svg>`;
  }

  return svg;
};

const svgFileFilter = (file) => file.mimeType === "image/svg+xml";
