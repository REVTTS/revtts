export class FinalizedComponent {
  /** @type {string} */
  name;
  /** @type {string} */
  path;

  /** @type {string} */
  html;

  /** @type {Array<FileObject>} */
  files;

  /**
   * @param {string} name
   * @param {string} path
   *
   * @param {string} html
   *
   * @param {Array<FileObject>} files
   */
  constructor(name, path, html, files) {
    this.name = name;
    this.path = path;
    this.html = html;
    this.files = files;
  }
}

/** @typedef {import('../../models/FileObject/FileObject.js').FileObject} FileObject */
