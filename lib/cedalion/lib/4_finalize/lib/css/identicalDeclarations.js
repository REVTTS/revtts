import { uniqueFilterFn } from "../../../uniqueFilterFn.js";
import { generateCss } from "../../../ast/css/generate.js";
import { parseCss } from "../../../ast/css/parse.js";
import { walkCss } from "../../../ast/css/walk.js";

/**
 * @param {Object} cssAst
 * @returns {Object}
 */
export const minifyCssIdenticalDeclarations = (cssAst) => {
  /** @type {Map<string, Array<string>>} */
  const duplicateDeclarations = new Map();
  walkCss(cssAst, {
    visit: "Rule",
    enter(ruleNode, item, list) {
      const preludeCss = generateCss(ruleNode.prelude);

      for (const declaration of ruleNode.block.children) {
        const declarationCss = generateCss(declaration);
        if (duplicateDeclarations.has(declarationCss)) {
          duplicateDeclarations
            .get(declarationCss)
            .push(...preludeCss.split(","));
        } else {
          duplicateDeclarations.set(declarationCss, [preludeCss]);
        }
      }

      list.remove(item);
    },
  });

  /** @type {Map<string, Array<string>>} */
  const duplicatePreludes = new Map();
  duplicateDeclarations.forEach((preludes, declarationCss) => {
    const selectorListCss = preludes
      .map(trimFn)
      .filter(uniqueFilterFn)
      .sort()
      .join(",");

    if (duplicatePreludes.has(selectorListCss)) {
      duplicatePreludes.get(selectorListCss).push(declarationCss);
    } else {
      duplicatePreludes.set(selectorListCss, [declarationCss]);
    }
  });

  const preludes = Array.from(duplicatePreludes.keys());
  for (let i = 0; i < preludes.length; i++) {
    let preludeArray1 = preludes[i].split(",");

    for (let j = 0; j < preludes.length; j++) {
      if (preludes[i] === preludes[j]) {
        continue;
      }
      const preludeArray2 = preludes[j].split(",");
      const preludeIntersection = arrayIntersection(
        preludeArray1,
        preludeArray2,
      );

      if (
        preludeIntersection.length <= preludeArray1.length / 2 ||
        preludeIntersection.length <= preludeArray2.length / 2
      ) {
        continue;
      }

      const declarations1 = duplicatePreludes.get(preludes[i]) || [];
      const declarations2 = duplicatePreludes.get(preludes[j]) || [];
      duplicatePreludes.delete(preludes[i]);
      duplicatePreludes.delete(preludes[j]);

      const newPrelude = preludeIntersection.join(",");
      const newDeclarations = duplicatePreludes.get(newPrelude) || [];
      newDeclarations.push(...declarations1, ...declarations2);
      duplicatePreludes.set(newPrelude, newDeclarations);

      const newPreludeArray1 = arrayDifference(
        preludeArray1,
        preludeIntersection,
      );
      if (newPreludeArray1.length > 0) {
        const newPrelude1 = newPreludeArray1.join(",");
        const newDeclarations1 = duplicatePreludes.get(newPrelude1) || [];
        newDeclarations1.push(...declarations1);

        duplicatePreludes.set(newPrelude1, newDeclarations1);
        preludeArray1 = newPreludeArray1;
        preludes[i] = newPrelude1;
      }

      const newPreludeArray2 = arrayDifference(
        preludeArray2,
        preludeIntersection,
      );
      if (newPreludeArray2.length > 0) {
        const newPrelude2 = newPreludeArray2.join(",");
        const newDeclarations2 = duplicatePreludes.get(newPrelude2) || [];
        newDeclarations2.push(...declarations2);

        duplicatePreludes.set(newPrelude2, newDeclarations2);
        preludes[j] = newPrelude2;
      }
    }
  }

  const newRules = [];
  duplicatePreludes.forEach((declarations, preludeCss) => {
    newRules.push(preludeCss + "{" + declarations.sort().join(";") + "}");
  });

  const generatedAst = parseCss(newRules.sort(sortRulesFn).join(""));
  cssAst.children.appendList(generatedAst.children);

  return cssAst;
};

/**
 * @template T
 * @param {Array<T>} array1
 * @param {Array<T>} array2
 * @return {Array<T>}
 */
const arrayIntersection = (array1, array2) =>
  array1.filter((value) => array2.includes(value));

/**
 * @template T
 * @param {Array<T>} array1
 * @param {Array<T>} array2
 * @return {Array<T>}
 */
const arrayDifference = (array1, array2) =>
  array1.filter((value) => !array2.includes(value));

/**
 * @param {string} str
 * @returns {string}
 */
const trimFn = (str) => str.trim();

/**
 * @param {string} a
 * @param {string} b
 */
const sortRulesFn = (a, b) => {
  const aStart = a.charAt(0);
  const bStart = b.charAt(0);

  if (aStart === bStart) {
    if (a < b) {
      return -1;
    } else if (a > b) {
      return 1;
    }

    return 0;
  }

  if (aStart === "@") {
    return -1;
  }
  if (bStart === "@") {
    return 1;
  }

  if (aStart === ":") {
    return -1;
  }
  if (bStart === ":") {
    return 1;
  }

  if (a < b) {
    return -1;
  } else if (a > b) {
    return 1;
  }

  return 0;
};
