import { generateCss } from "../../../ast/css/generate.js";
import { walkCss } from "../../../ast/css/walk.js";

/**
 * @param {Object} cssAst
 *
 * @returns {Object}
 */
export const minifyCssIdenticalRules = (cssAst) => {
  const nodeAccumulator = new Map();
  walkCss(cssAst, {
    visit: "Rule",
    enter(ruleNode, item, list) {
      const selector = generateCss(ruleNode.prelude);

      if (nodeAccumulator.has(selector)) {
        const existingNode = nodeAccumulator.get(selector);
        existingNode.block.children.appendList(ruleNode.block.children.copy());
        list.remove(item);
      } else {
        nodeAccumulator.set(selector, ruleNode);
      }
    },
  });

  return cssAst;
};
