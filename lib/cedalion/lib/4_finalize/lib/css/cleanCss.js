import { generateCss } from "../../../ast/css/generate.js";
import { parseCss } from "../../../ast/css/parse.js";
import { minifyCssIdenticalDeclarations } from "./identicalDeclarations.js";
import { minifyCssIdenticalRules } from "./identicalRules.js";

/**
 * @param {string} css
 * @returns {string}
 */
export const cleanCss = (css) => {
  css = css.replaceAll(commentRegex, "");
  css = css.replaceAll(newlineRegex, "\n");

  let cssAst = parseCss(css);
  cssAst = minifyCssIdenticalRules(cssAst);
  cssAst = minifyCssIdenticalDeclarations(cssAst);

  const newCss = generateCss(cssAst);
  minifyCssResultMap.set(css, newCss);
  return newCss;
};

/** @type {Map<string, Promise<string>>} */
const minifyCssResultMap = new Map();

const commentRegex = /\/\*.+\*\//g;
const newlineRegex = /\n+/g;
