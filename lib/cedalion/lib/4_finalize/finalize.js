import { generateHtml } from "../ast/html/generate.js";
import { parseHtml } from "../ast/html/parse.js";
import { FinalizedComponent } from "./lib/FinalizedComponent.js";
import { generateScript } from "./lib/generateScript.js";
import { generateStyle } from "./lib/generateStyle.js";
import { generateSvg } from "./lib/generateSvg.js";
import { getBodyAst } from "./lib/getBodyAst.js";
import { getHeadAst } from "./lib/getHeadAst.js";
import { hoistTemplates } from "./lib/hoistTemplates.js";
import { removeSlots } from "./lib/removeSlots.js";

/**
 * @param {BundledComponent} bundledComponent
 */
export const finalizeComponent = (bundledComponent) => {
  const htmlAst = parseHtml(bundledComponent.html);
  const files = Array.from(bundledComponent.files);

  const style = generateStyle(files);
  const svg = generateSvg(files);
  const script = generateScript(files);

  let docHtmlAst;

  if (htmlAst.nodeName === "#document") {
    docHtmlAst = htmlAst;
  } else {
    docHtmlAst = parseHtml(
      "<!doctype html>" +
        '<html lang="en-US">' +
        "<head>" +
        '<meta charset="utf-8" />' +
        '<meta name="viewport" content="width=device-width" />' +
        "</head>" +
        "<body>" +
        generateHtml(htmlAst) +
        "</body>" +
        "</html>",
    );
  }

  const headAst = getHeadAst(docHtmlAst);
  headAst.childNodes.push(
    ...parseHtml('<link rel="icon" href="/icons/favicon.ico" />').childNodes,
  );
  if (style !== "") {
    headAst.childNodes.push(...parseHtml(style).childNodes);
  }
  if (script !== "") {
    headAst.childNodes.push(...parseHtml(script).childNodes);
  }

  const bodyAst = getBodyAst(docHtmlAst);
  if (svg !== "") {
    bodyAst.childNodes.unshift(...parseHtml(svg).childNodes);
  }

  docHtmlAst = hoistTemplates(docHtmlAst, removeSlots);

  return new FinalizedComponent(
    bundledComponent.name,
    bundledComponent.path,
    generateHtml(docHtmlAst),
    files,
  );
};

/** @typedef {import('../3_bundle/lib/BundledComponent.js').BundledComponent} BundledComponent */
